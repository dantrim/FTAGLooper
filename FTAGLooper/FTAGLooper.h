#ifndef FTAGLooper_H
#define FTAGLooper_H

//ROOT
#include "TSelector.h"
class TTree;
class TFile;
#include "TH1F.h"

//std/stl
#include <string>
#include <vector>

//xAOD
namespace xAOD {
    class TEvent;
    class TStore;

    //class Vertex;
}
#include "xAODJet/Jet.h"
#include "xAODEgamma/Electron.h"
#include "AsgTools/AnaToolHandle.h"
#include "xAODTracking/Vertex.h"
#include "xAODTruth/TruthEventContainer.h"
class IAsgElectronLikelihoodTool;
namespace CP {
    class IEgammaCalibrationAndSmearingTool;
    class IIsolationCorrectionTool;
}
class IJetCalibrationTool;
class IJetSelector;
class IJetUpdateJvt;

const double mev2gev = 1.0e-3;

#undef CHECK
#define CHECK( ARG ) \
    do { \
        const bool result = ARG; \
        if( ! result ) { \
            ::Error("FTAGLooper", "Failed to execute: \"%s\"", \
                #ARG ); \
            exit(1); \
        } \
    } while (false)

class FTAGLooper : public TSelector
{
    public :
        FTAGLooper();
        virtual ~FTAGLooper();

        // config
        void set_debug(bool dbg) { m_dbg = dbg; }
        bool dbg() { return m_dbg; }

        void set_suffix(std::string suffix) { m_suffix = suffix; }
        std::string suffix() { return m_suffix; }

        // grabbers
        xAOD::TEvent* event() { return m_event; }
        xAOD::TStore* store() { return m_tstore; }

        TFile* output_file() { return m_output_tree_file; }
        TTree* output_tree() { return m_output_tree; }

        int dsid() { return m_dsid; }

        void initialize_tools();
        const xAOD::Vertex* GetPrimVtx();
        bool passJvt(const xAOD::Jet* jet);
        bool isBJet(const xAOD::Jet* jet, int wp = 85, bool dl1 = false);
        std::vector<float> get_dl1_tagger_weight(const xAOD::Jet* jet, float fraction = 0.08);
        bool jet_matches_electron(const xAOD::Jet* jet, const xAOD::Electron* electron,
                float dr_cut = 0.02, float pt_ratio_cut = 0.65);

        void save_output();
        void setup_output_tree();
        void initialize_branches(TTree* tree);
        void fill_jet_properties(std::vector<xAOD::Jet*> bjets,
            std::vector<xAOD::Jet*> bjets_em, std::vector<xAOD::Jet*> bjets_tm,
            std::vector<const xAOD::TruthParticle*> bhad,
            std::vector<const xAOD::TruthParticle*> chad,
            std::vector<const xAOD::TruthParticle*> tlep);
        void fill_jet_property(xAOD::Jet* bjet,
            std::vector<const xAOD::TruthParticle*> bhad,
            std::vector<const xAOD::TruthParticle*> chad,
            std::vector<const xAOD::TruthParticle*> tlep,
            std::string type = ""
            );

        bool particleInCollection(const xAOD::TrackParticle* trkParticle,
            std::vector<ElementLink<xAOD::TrackParticleContainer>> trkColl);
        float JF_Transverse_error(float L3D, float theta, float theta_err,
                float phi, float phi_err);
        std::vector<float> JF_xyz_errors(float L3D, float L3Derr, float theta, float theta_err,
                float phi, float phi_err, float PVx, float PVy, float PVz);

        bool get_metadata();

        //TSelector
        virtual Int_t Version() const { return 2; }
        virtual void Init(TTree* tree);
        virtual Bool_t Notify();
        virtual void Begin(TTree* tree);
        virtual void SlaveBegin(TTree* tree);
        virtual void Terminate();
        virtual Bool_t Process(Long64_t entry);

    private :
        bool m_dbg;
        bool m_output_setup;
        std::string m_suffix;
        int m_dsid;

        TFile* m_output_tree_file;
        TTree* m_output_tree;
        TTree* m_tree;
        xAOD::TEvent* m_event;
        xAOD::TStore* m_tstore;

        asg::AnaToolHandle<IJetCalibrationTool> m_jetCalibTool;
        asg::AnaToolHandle<IJetSelector> m_jetCleaningTool;
        asg::AnaToolHandle<IJetUpdateJvt> m_jetJvtUpdateTool;
        asg::AnaToolHandle<IAsgElectronLikelihoodTool> m_elecSelLikelihood;
        asg::AnaToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_egammaCalibTool;
        asg::AnaToolHandle<CP::IIsolationCorrectionTool> m_isoCorrTool;

        TH1F* h_drbe;
        TH1F* h_nbjets;
        TH1F* h_nbjets_truth_matched;
        TH1F* h_nbjets_ele_matched;

        ////////////////////////////////
        // TTree
        ////////////////////////////////

        // event
        int m_event_number;
        int m_mc_channel;
        double m_mc_weight;
        double m_avg_mu;
        double m_avg_mu_actual;
        double m_pv_x;
        double m_pv_y;
        double m_pv_z;
        double m_truth_pv_x;
        double m_truth_pv_y;
        double m_truth_pv_z;

        // jet properties
        int n_jet;
        std::vector<float> *v_jet_pt;
        std::vector<float> *v_jet_eta;
        std::vector<float> *v_jet_phi;
        std::vector<float> *v_jet_E;
        std::vector<float> *v_jet_JVT;
        std::vector<float> *v_jet_m;
        std::vector<float> *v_jet_nConst;
        std::vector<float> *v_jet_dRiso;
        std::vector<float> *v_jet_dRminToB;
        std::vector<float> *v_jet_dRminToC;
        std::vector<float> *v_jet_dRminToT;

        int n_jet_em;
        std::vector<float> *v_jet_pt_em;
        std::vector<float> *v_jet_eta_em;
        std::vector<float> *v_jet_phi_em;
        std::vector<float> *v_jet_E_em;
        std::vector<float> *v_jet_JVT_em;
        std::vector<float> *v_jet_m_em;
        std::vector<float> *v_jet_nConst_em;
        std::vector<float> *v_jet_dRiso_em;
        std::vector<float> *v_jet_dRminToB_em;
        std::vector<float> *v_jet_dRminToC_em;
        std::vector<float> *v_jet_dRminToT_em;

        int n_jet_tm;
        std::vector<float> *v_jet_pt_tm;
        std::vector<float> *v_jet_eta_tm;
        std::vector<float> *v_jet_phi_tm;
        std::vector<float> *v_jet_E_tm;
        std::vector<float> *v_jet_JVT_tm;
        std::vector<float> *v_jet_m_tm;
        std::vector<float> *v_jet_nConst_tm;
        std::vector<float> *v_jet_dRiso_tm;
        std::vector<float> *v_jet_dRminToB_tm;
        std::vector<float> *v_jet_dRminToC_tm;
        std::vector<float> *v_jet_dRminToT_tm;

        // tagger scores
        std::vector<double> *v_jet_dl1_pb;
        std::vector<double> *v_jet_dl1_pc;
        std::vector<double> *v_jet_dl1_pu;
        std::vector<double> *v_jet_dl1;
        std::vector<double> *v_jet_mv2c10;

        std::vector<double> *v_jet_dl1_pb_em;
        std::vector<double> *v_jet_dl1_pc_em;
        std::vector<double> *v_jet_dl1_pu_em;
        std::vector<double> *v_jet_dl1_em;
        std::vector<double> *v_jet_mv2c10_em;

        std::vector<double> *v_jet_dl1_pb_tm;
        std::vector<double> *v_jet_dl1_pc_tm;
        std::vector<double> *v_jet_dl1_pu_tm;
        std::vector<double> *v_jet_dl1_tm;
        std::vector<double> *v_jet_mv2c10_tm;

        // SV1 stuff
        std::vector<int>* v_sv1_NVtx;
        std::vector<float>* v_sv1_NGTinSvx;
        std::vector<float>* v_sv1_N2Tpair;
        std::vector<float>* v_sv1_masssvx;
        std::vector<float>* v_sv1_efracsvx;
        std::vector<float>* v_sv1_normdist;
        std::vector<float>* v_sv1_significance3d;
        std::vector<float>* v_sv1_deltaR;
        std::vector<float>* v_sv1_Lxy;
        std::vector<float>* v_sv1_L3d;
    
        std::vector<int>* v_sv1_NVtx_em;
        std::vector<float>* v_sv1_NGTinSvx_em;
        std::vector<float>* v_sv1_N2Tpair_em;
        std::vector<float>* v_sv1_masssvx_em;
        std::vector<float>* v_sv1_efracsvx_em;
        std::vector<float>* v_sv1_normdist_em;
        std::vector<float>* v_sv1_significance3d_em;
        std::vector<float>* v_sv1_deltaR_em;
        std::vector<float>* v_sv1_Lxy_em;
        std::vector<float>* v_sv1_L3d_em;

        std::vector<int>* v_sv1_NVtx_tm;
        std::vector<float>* v_sv1_NGTinSvx_tm;
        std::vector<float>* v_sv1_N2Tpair_tm;
        std::vector<float>* v_sv1_masssvx_tm;
        std::vector<float>* v_sv1_efracsvx_tm;
        std::vector<float>* v_sv1_normdist_tm;
        std::vector<float>* v_sv1_significance3d_tm;
        std::vector<float>* v_sv1_deltaR_tm;
        std::vector<float>* v_sv1_Lxy_tm;
        std::vector<float>* v_sv1_L3d_tm;

        // JetFitter stuff
        float m_jf_PV_x;
        float m_jf_PV_y;
        float m_jf_PV_z;
        float m_jf_PV_x_em;
        float m_jf_PV_y_em;
        float m_jf_PV_z_em;
        float m_jf_PV_x_tm;
        float m_jf_PV_y_tm;
        float m_jf_PV_z_tm;

        std::vector<std::vector<int> >* v_jf_jet_trk_vertex;
        std::vector<float>* v_jf_jet_pb;
        std::vector<float>* v_jf_jet_pc;
        std::vector<float>* v_jf_jet_pu;
        std::vector<float>* v_jf_jet_llr;
        std::vector<float>* v_jf_jet_m;
        std::vector<float>* v_jf_jet_efc;
        std::vector<float>* v_jf_jet_deta;
        std::vector<float>* v_jf_jet_dphi;
        std::vector<float>* v_jf_jet_dR;
        std::vector<float>* v_jf_jet_ntrkAtVx;
        std::vector<float>* v_jf_jet_nvtx;
        std::vector<float>* v_jf_jet_sig3d;
        std::vector<float>* v_jf_jet_nvtx1t;
        std::vector<float>* v_jf_jet_n2t;
        std::vector<float>* v_jf_jet_VTXsize;
        std::vector<std::vector<float> >* v_jf_jet_vtx_chi2;
        std::vector<std::vector<float> >* v_jf_jet_vtx_ndf;
        std::vector<std::vector<float> >* v_jf_jet_vtx_ntrk;
        std::vector<std::vector<float> >* v_jf_jet_vtx_L3d;
        std::vector<std::vector<float> >* v_jf_jet_vtx_sig3d;
        std::vector<std::vector<float> >* v_jf_jet_vtx_x;
        std::vector<std::vector<float> >* v_jf_jet_vtx_x_err;
        std::vector<std::vector<float> >* v_jf_jet_vtx_y;
        std::vector<std::vector<float> >* v_jf_jet_vtx_y_err;
        std::vector<std::vector<float> >* v_jf_jet_vtx_z;
        std::vector<std::vector<float> >* v_jf_jet_vtx_z_err;

        std::vector<std::vector<int> >* v_jf_jet_trk_vertex_em;
        std::vector<float>* v_jf_jet_pb_em;
        std::vector<float>* v_jf_jet_pc_em;
        std::vector<float>* v_jf_jet_pu_em;
        std::vector<float>* v_jf_jet_llr_em;
        std::vector<float>* v_jf_jet_m_em;
        std::vector<float>* v_jf_jet_efc_em;
        std::vector<float>* v_jf_jet_deta_em;
        std::vector<float>* v_jf_jet_dphi_em;
        std::vector<float>* v_jf_jet_dR_em;
        std::vector<float>* v_jf_jet_ntrkAtVx_em;
        std::vector<float>* v_jf_jet_nvtx_em;
        std::vector<float>* v_jf_jet_sig3d_em;
        std::vector<float>* v_jf_jet_nvtx1t_em;
        std::vector<float>* v_jf_jet_n2t_em;
        std::vector<float>* v_jf_jet_VTXsize_em;
        std::vector<std::vector<float> >* v_jf_jet_vtx_chi2_em;
        std::vector<std::vector<float> >* v_jf_jet_vtx_ndf_em;
        std::vector<std::vector<float> >* v_jf_jet_vtx_ntrk_em;
        std::vector<std::vector<float> >* v_jf_jet_vtx_L3d_em;
        std::vector<std::vector<float> >* v_jf_jet_vtx_sig3d_em;
        std::vector<std::vector<float> >* v_jf_jet_vtx_x_em;
        std::vector<std::vector<float> >* v_jf_jet_vtx_x_err_em;
        std::vector<std::vector<float> >* v_jf_jet_vtx_y_em;
        std::vector<std::vector<float> >* v_jf_jet_vtx_y_err_em;
        std::vector<std::vector<float> >* v_jf_jet_vtx_z_em;
        std::vector<std::vector<float> >* v_jf_jet_vtx_z_err_em;

        std::vector<std::vector<int> >* v_jf_jet_trk_vertex_tm;
        std::vector<float>* v_jf_jet_pb_tm;
        std::vector<float>* v_jf_jet_pc_tm;
        std::vector<float>* v_jf_jet_pu_tm;
        std::vector<float>* v_jf_jet_llr_tm;
        std::vector<float>* v_jf_jet_m_tm;
        std::vector<float>* v_jf_jet_efc_tm;
        std::vector<float>* v_jf_jet_deta_tm;
        std::vector<float>* v_jf_jet_dphi_tm;
        std::vector<float>* v_jf_jet_dR_tm;
        std::vector<float>* v_jf_jet_ntrkAtVx_tm;
        std::vector<float>* v_jf_jet_nvtx_tm;
        std::vector<float>* v_jf_jet_sig3d_tm;
        std::vector<float>* v_jf_jet_nvtx1t_tm;
        std::vector<float>* v_jf_jet_n2t_tm;
        std::vector<float>* v_jf_jet_VTXsize_tm;
        std::vector<std::vector<float> >* v_jf_jet_vtx_chi2_tm;
        std::vector<std::vector<float> >* v_jf_jet_vtx_ndf_tm;
        std::vector<std::vector<float> >* v_jf_jet_vtx_ntrk_tm;
        std::vector<std::vector<float> >* v_jf_jet_vtx_L3d_tm;
        std::vector<std::vector<float> >* v_jf_jet_vtx_sig3d_tm;
        std::vector<std::vector<float> >* v_jf_jet_vtx_x_tm;
        std::vector<std::vector<float> >* v_jf_jet_vtx_x_err_tm;
        std::vector<std::vector<float> >* v_jf_jet_vtx_y_tm;
        std::vector<std::vector<float> >* v_jf_jet_vtx_y_err_tm;
        std::vector<std::vector<float> >* v_jf_jet_vtx_z_tm;
        std::vector<std::vector<float> >* v_jf_jet_vtx_z_err_tm;

        // vertex closest to primary
        std::vector<float>* v_jf_nTrk_vtx1;
        std::vector<float>* v_jf_mass_first_vtx;
        std::vector<float>* v_jf_e_first_vtx;
        std::vector<float>* v_jf_e_frac_vtx1;
        std::vector<float>* v_jf_closestVtx_L3D;
        std::vector<float>* v_jf_JF_Lxy1;
        std::vector<float>* v_jf_vtx1_MaxTrkRapidity_jf_path;
        std::vector<float>* v_jf_vtx1_AvgTrkRapidity_jf_path;
        std::vector<float>* v_jf_vtx1_MinTrkRapidity_jf_path;

        std::vector<float>* v_jf_nTrk_vtx1_em;
        std::vector<float>* v_jf_mass_first_vtx_em;
        std::vector<float>* v_jf_e_first_vtx_em;
        std::vector<float>* v_jf_e_frac_vtx1_em;
        std::vector<float>* v_jf_closestVtx_L3D_em;
        std::vector<float>* v_jf_JF_Lxy1_em;
        std::vector<float>* v_jf_vtx1_MaxTrkRapidity_jf_path_em;
        std::vector<float>* v_jf_vtx1_AvgTrkRapidity_jf_path_em;
        std::vector<float>* v_jf_vtx1_MinTrkRapidity_jf_path_em;

        std::vector<float>* v_jf_nTrk_vtx1_tm;
        std::vector<float>* v_jf_mass_first_vtx_tm;
        std::vector<float>* v_jf_e_first_vtx_tm;
        std::vector<float>* v_jf_e_frac_vtx1_tm;
        std::vector<float>* v_jf_closestVtx_L3D_tm;
        std::vector<float>* v_jf_JF_Lxy1_tm;
        std::vector<float>* v_jf_vtx1_MaxTrkRapidity_jf_path_tm;
        std::vector<float>* v_jf_vtx1_AvgTrkRapidity_jf_path_tm;
        std::vector<float>* v_jf_vtx1_MinTrkRapidity_jf_path_tm;


        // vertex second closest to primary
        std::vector<float>* v_jf_nTrk_vtx2;
        std::vector<float>* v_jf_mass_second_vtx;
        std::vector<float>* v_jf_e_second_vtx;
        std::vector<float>* v_jf_e_frac_vtx2;
        std::vector<float>* v_jf_second_closestVtx_L3D;
        std::vector<float>* v_jf_JF_Lxy2;
        std::vector<float>* v_jf_vtx2_MaxTrkRapidity_jf_path;
        std::vector<float>* v_jf_vtx2_AvgTrkRapidity_jf_path;
        std::vector<float>* v_jf_vtx2_MinTrkRapidity_jf_path;


        std::vector<float>* v_jf_nTrk_vtx2_em;
        std::vector<float>* v_jf_mass_second_vtx_em;
        std::vector<float>* v_jf_e_second_vtx_em;
        std::vector<float>* v_jf_e_frac_vtx2_em;
        std::vector<float>* v_jf_second_closestVtx_L3D_em;
        std::vector<float>* v_jf_JF_Lxy2_em;
        std::vector<float>* v_jf_vtx2_MaxTrkRapidity_jf_path_em;
        std::vector<float>* v_jf_vtx2_AvgTrkRapidity_jf_path_em;
        std::vector<float>* v_jf_vtx2_MinTrkRapidity_jf_path_em;

        std::vector<float>* v_jf_nTrk_vtx2_tm;
        std::vector<float>* v_jf_mass_second_vtx_tm;
        std::vector<float>* v_jf_e_second_vtx_tm;
        std::vector<float>* v_jf_e_frac_vtx2_tm;
        std::vector<float>* v_jf_second_closestVtx_L3D_tm;
        std::vector<float>* v_jf_JF_Lxy2_tm;
        std::vector<float>* v_jf_vtx2_MaxTrkRapidity_jf_path_tm;
        std::vector<float>* v_jf_vtx2_AvgTrkRapidity_jf_path_tm;
        std::vector<float>* v_jf_vtx2_MinTrkRapidity_jf_path_tm;


        // all tracks in the jet
        std::vector<float>* v_jf_MaxTrkRapidity_jf_path;
        std::vector<float>* v_jf_MinTrkRapidity_jf_path;
        std::vector<float>* v_jf_AvgTrkRapidity_jf_path;
        std::vector<float>* v_jf_MaxTrkRapidity_jf_path_em;
        std::vector<float>* v_jf_MinTrkRapidity_jf_path_em;
        std::vector<float>* v_jf_AvgTrkRapidity_jf_path_em;
        std::vector<float>* v_jf_MaxTrkRapidity_jf_path_tm;
        std::vector<float>* v_jf_MinTrkRapidity_jf_path_tm;
        std::vector<float>* v_jf_AvgTrkRapidity_jf_path_tm;

        // IP stuff
        std::vector<float>* v_jet_ip2d_pb;
        std::vector<float>* v_jet_ip2d_pc;
        std::vector<float>* v_jet_ip2d_pu;
        std::vector<float>* v_jet_ip2d_llr;
        std::vector<float>* v_jet_ip3d_pb;
        std::vector<float>* v_jet_ip3d_pc;
        std::vector<float>* v_jet_ip3d_pu;
        std::vector<float>* v_jet_ip3d_llr;

        std::vector<float>* v_jet_ip2d_pb_em;
        std::vector<float>* v_jet_ip2d_pc_em;
        std::vector<float>* v_jet_ip2d_pu_em;
        std::vector<float>* v_jet_ip2d_llr_em;
        std::vector<float>* v_jet_ip3d_pb_em;
        std::vector<float>* v_jet_ip3d_pc_em;
        std::vector<float>* v_jet_ip3d_pu_em;
        std::vector<float>* v_jet_ip3d_llr_em;

        std::vector<float>* v_jet_ip2d_pb_tm;
        std::vector<float>* v_jet_ip2d_pc_tm;
        std::vector<float>* v_jet_ip2d_pu_tm;
        std::vector<float>* v_jet_ip2d_llr_tm;
        std::vector<float>* v_jet_ip3d_pb_tm;
        std::vector<float>* v_jet_ip3d_pc_tm;
        std::vector<float>* v_jet_ip3d_pu_tm;
        std::vector<float>* v_jet_ip3d_llr_tm;

    //ClassDef(FTAGLooper, 0);
};

#endif
