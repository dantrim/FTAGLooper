#include <iostream>
using namespace std;
#include <string>

//boost
#include "boost/program_options.hpp"
namespace po = boost::program_options;

//xAOD
#include "xAODRootAccess/Init.h"

//FTAGLooper
#include "FTAGLooper/FTAGLooper.h"
#include "FTAGLooper/ChainHelper.h"
#include "FTAGLooper/string_utils.h"

int main(int argc, char* argv[])
{

    string input_file_name = "";
    int n_events_to_process = 0;
    string job_suffix = "";
    bool debug = false;

    try {

        po::options_description desc("FTAGLooper - loopFtag");
        desc.add_options()
        ("input-file,i", po::value<std::string>(&input_file_name)->required(), "Provide the input FTAG derivation file")
        ("nevents,n", po::value<int>(&n_events_to_process)->default_value(-1), "Provide the number of events to process")
        ("suffix,s", po::value<std::string>(&job_suffix)->default_value(""), "Provide a job suffix")
        ("dbg,d", "Turn on debug")
        ("help,h", "Print this help message")
        ;

        po::variables_map vm;
        try {

            po::store(po::parse_command_line(argc, argv, desc), vm);

            if(vm.count("help")) {
                cout << desc << endl;
                return 0;
            }

            if(vm.count("dbg")) {
                debug = true;
            }

            po::notify(vm);
        }
        catch(po::error& e)
        {
            cout << "loopFtag    ERROR parsing command line: " << e.what() << endl;
            return 1;
        }

        cout << "-----------------------------------------" << endl;
        cout << " loopFtag run options\n" << endl;
        cout << " input file          : " << vm["input-file"].as<std::string>() << endl;
        cout << " n events to process : " << vm["nevents"].as<int>() << endl;
        cout << " suffix              : " << vm["suffix"].as<std::string>() << endl;
        cout << " debug               : " << ( ((vm.count("dbg") > 0)) ? "true" : "false") << endl;
        cout << "-----------------------------------------" << endl;

    }
    catch(std::exception& e)
    {
        cout << "loopFtag    ERROR: " << e.what() << endl;
        return 1;
    }

    xAOD::Init("loopFtag");
    TChain* chain = new TChain("CollectionTree");
    int file_err = ChainHelper::addInput(chain, input_file_name, true);
    if(file_err) return 1;
    chain->ls();
    Long64_t n_entries = chain->GetEntries();

    FTAGLooper* looper = new FTAGLooper();
    looper->set_debug(debug);
    looper->set_suffix(job_suffix);
    if(n_events_to_process < 0) n_events_to_process = n_entries;
    chain->Process(looper, "", n_events_to_process);
    delete chain;
    delete looper;
    return 0;


    return 0;
}
