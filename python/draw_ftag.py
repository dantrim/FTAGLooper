#!/usr/bin/env/python

import sys
import ROOT as r
r.gStyle.SetOptStat(False)

def get_binning(varname) :

    binning = {}
    binning["n_jet"] = [6, 0, 6]
    binning["v_jet_pt"] = [50, 0, 1000]
    binning["v_jet_eta"] = [80, -3.2, 3.2]
    binning["v_jet_phi"] = [80, -3.2, 3.2]
    binning["v_jet_E"] = [60, 0, 2500]
    binning["v_jet_JVT"] = [25, 0, 1]
    binning["v_jet_m"] = [30, 0, 300]
    binning["v_jet_nConst"] = [40, 0, 40]
    binning["v_jet_dRiso"] = [50, 0, 5]
    binning["v_jet_dRminToB"] = [50, 0, 5]
    binning["v_jet_dRminToC"] = [50, 0, 5]
    binning["v_jet_dRminToT"] = [50, 0, 10]
    binning["v_jet_dl1_pb"] = [50, 0, 1]
    binning["v_jet_dl1_pc"] = [50, 0, 1]
    binning["v_jet_dl1_pu"] = [50, 0, 1]
    binning["v_jet_dl1"] = [35, -4, 10]
    binning["v_jet_mv2c10"] = [50, 0, 1]
    binning["v_sv1_NVtx"] = [40, -1, 3]
    binning["v_sv1_NGTinSvx"] = [20, 0, 20]
    binning["v_sv1_N2Tpair"] = [40, 0, 40]
    binning["v_sv1_masssvx"] = [50, 0, 10]
    binning["v_sv1_efracsvx"] = [50, 0, 1]
    binning["v_sv1_normdist"] = [50, 0, 500]
    binning["v_sv1_significance3d"] = [25, 0, 500]
    binning["v_sv1_deltaR"] = [20, 0, 4]
    binning["v_sv1_Lxy"] = [30, 0, 150]
    binning["v_sv1_L3d"] = [30, 0, 300]
    binning["v_jf_jet_trk_vertex"] = [12, 0, 12]
    binning["v_jf_jet_pb"] = [50, 0, 1]
    binning["v_jf_jet_pc"] = [50, 0, 1]
    binning["v_jf_jet_pu"] = [50, 0, 1]
    binning["v_jf_jet_llr"] = [50, -10, 10]
    binning["v_jf_jet_m"] = [50, 0, 10]
    binning["v_jf_jet_efc"] = [25, 0, 1]
    binning["v_jf_jet_deta"] = [60, -0.6, 0.6]
    binning["v_jf_jet_dphi"] = [60, -0.5, 0.5]
    binning["v_jf_jet_dR"] = [100, 0, 0.6] # why are there values at dR==14??
    binning["v_jf_jet_nvtx"] = [10, 0, 10]
    binning["v_jf_jet_sig3d"] = [50, 0, 100]
    binning["v_jf_jet_nvtx1t"] = [10, 0, 10]
    binning["v_jf_jet_VTXsize"] = [15, 0, 15]
    binning["v_jf_jet_vtx_chi2"] = [40, 0, 40]
    binning["v_jf_jet_vtx_ndf"] = [30, 0, 30]
    binning["v_jf_jet_vtx_ntrk"] = [30, 0, 30]
    binning["v_jf_jet_vtx_L3d"] = [50, -1000, 1000]
    binning["v_jf_jet_vtx_sig3d"] = [60, 0, 600]
    binning["v_jf_jet_vtx_x"] = [40, -400, 400]
    binning["v_jf_jet_vtx_x_err"] = [50, 0, 400]
    binning["v_jf_jet_vtx_y"] = [40, -400, 400]
    binning["v_jf_jet_vtx_y_err"] = [50, 0, 400]
    binning["v_jf_jet_vtx_z"] = [40, -400, 400]
    binning["v_jf_jet_vtx_z_err"] = [50, 0, 400]
    binning["v_jf_e_first_vtx"] = [50, 0, 1000]
    binning["v_jf_e_frac_vtx1"] = [50, 0, 1]
    binning["v_jf_closestVtx_L3D"] = [100, 0, 1000]
    binning["v_jf_JF_Lxy1"] = [50, 0, 400]
    binning["v_jf_vtx1_MaxTrkRapidity_jf_path"] = [50, 0, 20]
    binning["v_jf_vtx1_AvgTrkRapidity_jf_path"] = [50, 0, 20]
    binning["v_jf_vtx1_MinTrkRapidity_jf_path"] = [50, 0, 20]
    binning["v_jf_nTrk_vtx2"] = [12, 0, 12] 
    binning["v_jf_mass_second_vtx"] = [50, 0, 8]
    binning["v_jf_e_second_vtx"] = [50, 0, 1000]
    binning["v_jf_e_frac_vtx2"] = [25, 0, 1]
    binning["v_jf_second_closestVtx_L3D"] = [50, 0, 1000]
    binning["v_jf_JF_Lxy2"] = [50, 0, 400]
    binning["v_jf_vtx2_MaxTrkRapidity_jf_path"] = [50, 0, 20]
    binning["v_jf_vtx2_AvgTrkRapidity_jf_path"] = [50, 0, 20]
    binning["v_jf_vtx2_MinTrkRapidity_jf_path"] = [50, 0, 20]
    binning["v_jf_MaxTrkRapidity_jf_path"] = [50, 0, 20]
    binning["v_jf_AvgTrkRapidity_jf_path"] = [50, 0, 20]
    binning["v_jf_MinTrkRapidity_jf_path"] = [50, 0, 20]
    binning["v_jet_ip2d_pb"] = [25, 0, 1]
    binning["v_jet_ip2d_pc"] = [25, 0, 1]
    binning["v_jet_ip2d_pu"] = [25, 0, 1]
    binning["v_jet_ip2d_llr"] = [100, -20, 30]

    binning["v_jet_ip3d_pb"] = [25, 0, 1]
    binning["v_jet_ip3d_pc"] = [25, 0, 1]
    binning["v_jet_ip3d_pu"] = [25, 0, 1]
    binning["v_jet_ip3d_llr"] = [100, -20, 30]



    if varname not in binning :
        out = []
        return out

    return binning[varname]

def plot_var(tree, var_name) :

    print "plot_var    %s" % var_name

    bin_info = get_binning(var_name)
    if len(bin_info) == 0 :
        return 

    #hb = r.TH1F("hb_%s" % var_name, ";%s;a.u." % var_name, 1000, 0, -1)
    #he = r.TH1F("he_%s" % var_name, ";%s;a.u." % var_name, 1000, 0, -1)
    #ht = r.TH1F("ht_%s" % var_name, ";%s;a.u." % var_name, 1000, 0, -1)
    hb = r.TH1F("hb_%s" % var_name, ";%s;a.u." % var_name, bin_info[0],bin_info[1],bin_info[2])
    he = r.TH1F("he_%s" % var_name, ";%s;a.u." % var_name, bin_info[0],bin_info[1],bin_info[2])
    ht = r.TH1F("ht_%s" % var_name, ";%s;a.u." % var_name, bin_info[0],bin_info[1],bin_info[2])
    histos = [hb, he, ht]
    for h in histos :
        h.SetLineWidth(2)

    hb.SetLineColor(r.kBlack)
    he.SetLineColor(r.kRed)
    ht.SetLineColor(r.kBlue)

    cmd = "%s>>%s" % (var_name, hb.GetName())
    tree.Draw(cmd, "", "goff")
    cmd = "%s_em>>%s" % (var_name, he.GetName())
    tree.Draw(cmd, "", "goff")
    cmd = "%s_tm>>%s" % (var_name, ht.GetName())
    tree.Draw(cmd, "", "goff")

    maxy = -1
    for h in histos :
        if h.Integral() == 0 :
            print "skipping %s" % var_name
            return
        h.Scale(1/h.Integral())
    for h in histos :
        if h.GetMaximum() > maxy : maxy = h.GetMaximum()

    do_log = True
    if do_log :
        maxy = 10 * maxy
    else :
        maxy = 1.3 * maxy
    #maxy = 10
    for h in histos :
        h.SetMaximum(maxy)

    c = r.TCanvas("c_%s" % var_name, "", 800, 600)
    c.SetLogy(do_log)
    c.cd()
    if do_log :
        for h in histos :
            h.SetMinimum(1e-5)
    for ih, h in enumerate(histos) :
        cmd = "hist"
        if ih != 0 : cmd += " same"
        h.Draw(cmd)
        c.Update()
    x = raw_input()
    c.Close()

def plot_var_prw(tree, var_name) :

    bin_info = get_binning(var_name)
    if len(bin_info) == 0 :
        return
   #o mu_cuts = ["avg_mu>0 && avg_mu<10", "avg_mu>10 && avg_mu<20", "avg_mu>20 && avg_mu<30", "avg_mu>30"]
    mu_cuts = ["avg_mu<15", "avg_mu>15 && avg_mu<30", "avg_mu>30"]

    maxy = -1

    histos_low = []
    histos_med = []
    histos_high = []
    for imu, mu_cut in enumerate(mu_cuts) :
        hb = r.TH1F("hb_%d_%s" % (imu, var_name), ";%s;a.u." % var_name,
            bin_info[0], bin_info[1], bin_info[2])
        he = r.TH1F("he_%d_%s" % (imu, var_name), ";%s;a.u." % var_name,
            bin_info[0], bin_info[1], bin_info[2])
        ht = r.TH1F("ht_%d_%s" % (imu, var_name), ";%s;a.u." % var_name,
            bin_info[0], bin_info[1], bin_info[2])

        hb.SetLineWidth(2)
        he.SetLineWidth(2)
        ht.SetLineWidth(2)
        if imu == 0 :
            hb.SetLineStyle(1)
            he.SetLineStyle(1)
            ht.SetLineStyle(1)
        elif imu == 1 :
            hb.SetLineStyle(2)
            he.SetLineStyle(2)
            ht.SetLineStyle(2)
        elif imu == 2 :
            hb.SetLineStyle(3)
            he.SetLineStyle(3)
            ht.SetLineStyle(3)
        hb.SetLineColor(r.kBlack)
        he.SetLineColor(r.kRed)
        ht.SetLineColor(r.kBlue)

        cmd = "%s>>%s" % (var_name, hb.GetName())
        cut = r.TCut(mu_cut)
        sel = r.TCut("1")
        tree.Draw(cmd, cut * sel, "goff")
        cmd = "%s_em>>%s" % (var_name, he.GetName())
        tree.Draw(cmd, cut * sel, "goff")
        cmd = "%s_tm>>%s" % (var_name, ht.GetName())
        tree.Draw(cmd, cut * sel, "goff")

        if imu == 0 :
            histos_low.append(hb)
            histos_low.append(he)
            histos_low.append(ht)
        elif imu == 1 :
            histos_med.append(hb)
            histos_med.append(he)
            histos_med.append(ht)
        elif imu == 2 :
            histos_high.append(hb)
            histos_high.append(he)
            histos_high.append(ht)

    for h in histos_low :
        if h.Integral() == 0 :
            print "skipping %s" % var_name
            return
        h.Scale(1/h.Integral()) 

    for h in histos_med :
        if h.Integral() == 0 :
            print "skipping %s" % var_name
            return
        h.Scale(1/h.Integral()) 

    for h in histos_high :
        if h.Integral() == 0 :
            print "skipping %s" % var_name
            return
        h.Scale(1/h.Integral()) 

    for h in histos_low :
        if h.GetMaximum() > maxy : maxy = h.GetMaximum()
    for h in histos_med :
        if h.GetMaximum() > maxy : maxy = h.GetMaximum()
    for h in histos_high :
        if h.GetMaximum() > maxy : maxy = h.GetMaximum()

    do_log = True
    if do_log :
        maxy = 10 * maxy
    else :
        maxy = 1.3 * maxy

    for h in histos_low :
        h.SetMaximum(maxy)
    for h in histos_med :
        h.SetMaximum(maxy)
    for h in histos_high :
        h.SetMaximum(maxy)

    c = r.TCanvas("c_%s" % var_name, "", 800, 600)
    c.SetLogy(do_log)
    c.cd()
    all_histos = [h for h in histos_low]
    all_histos += [h for h in histos_med]
    all_histos += [h for h in histos_high]
    if do_log :
        for h in all_histos :
            h.SetMinimum(1e-5)
    for ih, h in enumerate(all_histos) :
        cmd = "hist"
        if ih != 0 : cmd += " same"
        h.Draw(cmd)
        c.Update()
    x = raw_input()
    c.Close()

    

def main() :
    input_file = sys.argv[1]
    var_select = ""
    if len(sys.argv) > 2 :
        var_select = sys.argv[2]

    rfile = r.TFile.Open(input_file)
    tree = rfile.Get("ftag")
    branch_list = tree.GetListOfBranches()
    all_variable_names = []
    for branch in branch_list :
        all_variable_names.append(branch.GetName())
    all_variable_names = [v for v in all_variable_names if "_em" not in v]
    all_variable_names = [v for v in all_variable_names if "_tm" not in v]

    print all_variable_names
    if var_select != "" :
        if var_select in all_variable_names :
            all_variable_names = [var_select]
        else :
            print "requested variable (=%s) not in list" % var_select
            sys.exit()

    if len(all_variable_names) == 1 :
        plot_var(tree, var_select)
    else :
        for v in all_variable_names[11:] :
            plot_var(tree, v)
    #if len(all_variable_names) == 1 :
    #    plot_var_prw(tree, var_select)
    #else :
    #    for v in all_variable_names[11:] :
    #        plot_var_prw(tree, v)

    

if __name__ == "__main__" :
    main()
