#include "FTAGLooper/FTAGLooper.h"

//std/stl
#include <iostream>
#include <cmath>
#include <sstream>
using namespace std;

//ROOT
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TVector3.h"
#include "TBranch.h"

//xAOD/EDM
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "xAODEventInfo/EventInfo.h"
#include <xAODCore/ShallowCopy.h>
#include <xAODBase/IParticleHelpers.h>

#include "xAODTruth/TruthEventContainer.h"
///#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/TrackingPrimitives.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
//#include "ParticleJetTools/JetFlavourInfo.h"
#include "xAODBTagging/SecVtxHelper.h"
#include "xAODBTagging/BTagging.h"
#include "xAODBTagging/BTagVertex.h"

#include "JetInterface/IJetSelector.h"
#include "xAODEventShape/EventShape.h"

// Tool handles
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetInterface/IJetUpdateJvt.h"
#include "EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h"
#include "EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h"
#include "IsolationCorrections/IIsolationCorrectionTool.h"

const static SG::AuxElement::Decorator<float> dec_jvt("Jvt");
const static SG::AuxElement::Accessor<float> acc_jvt("Jvt");

const static SG::AuxElement::Decorator<float>     dec_z0sinTheta("z0sinTheta");
const static SG::AuxElement::ConstAccessor<float> acc_z0sinTheta("z0sinTheta");
const static SG::AuxElement::Decorator<float>     dec_d0sig("d0sig");
const static SG::AuxElement::ConstAccessor<float> acc_d0sig("d0sig");

typedef std::vector<ElementLink<xAOD::VertexContainer > > Vertices;
SG::AuxElement::ConstAccessor<Vertices> acc_sv1_vertices("SV1_vertices");
SG::AuxElement::ConstAccessor<float> acc_sv1_efracsvx("SV1_efracsvx");

const float mv2_85_cut = 0.108029;
const float mv2_77_cut = 0.643362;
const float dl1_85_cut = 0.463453;
const float dl1_77_cut = 1.44686;

//SV1 accessor
typedef std::vector<ElementLink<xAOD::VertexContainer > > Vertices;
const static SG::AuxElement::ConstAccessor<Vertices> acc_SV1_Vertices("SV1_vertices");
const static SG::AuxElement::ConstAccessor<int> acc_SV1_NGTinSvx("SV1_NGTinSvx");
const static SG::AuxElement::ConstAccessor<int> acc_SV1_N2Tpair("SV1_N2Tpair");
const static SG::AuxElement::ConstAccessor<float> acc_SV1_masssvx("SV1_masssvx");
const static SG::AuxElement::ConstAccessor<float> acc_SV1_efracsvx("SV1_efracsvx");
const static SG::AuxElement::ConstAccessor<float> acc_SV1_normdist("SV1_normdist");
const static SG::AuxElement::ConstAccessor<float> acc_SV1_significance3d("SV1_significance3d");
const static SG::AuxElement::ConstAccessor<float> acc_SV1_deltaR("SV1_deltaR");
const static SG::AuxElement::ConstAccessor<float> acc_SV1_Lxy("SV1_Lxy");
const static SG::AuxElement::ConstAccessor<float> acc_SV1_L3d("SV1_L3d");

//////////////////////////////////////////////////////////////////////////////
FTAGLooper::FTAGLooper() :
    m_dbg(false),
    m_output_setup(false),
    m_suffix(""),
    m_dsid(0),
    m_output_tree_file(nullptr),
    m_output_tree(nullptr),
    m_tree(nullptr),
    m_event(new xAOD::TEvent(xAOD::TEvent::kClassAccess) ),
    m_tstore(),
    m_jetCalibTool(""),
    m_jetCleaningTool(""),
    m_jetJvtUpdateTool("")
{

}
//////////////////////////////////////////////////////////////////////////////
void FTAGLooper::Init(TTree* tree)
{
    cout << "FTAGLooper::Init" << endl;

    if(tree) {
        cout << "FTAGLooper::Init    Found tree with " << tree->GetEntries() << " entries" << endl;
    }
    else {
        cout << "FTAGLooper::Init    ERROR Input tree is null" << endl;
        exit(1);
    }
    m_tree = tree;
    m_event->readFrom(tree);

    h_drbe = new TH1F("h_drbe", ";DR;", 600, 0, 6);
    h_nbjets = new TH1F("h_nbjets", ";N bjets", 5, 0, 5);
    h_nbjets_truth_matched = new TH1F("h_nbjets_truth_matched", ";N bjets truth matched", 5, 0, 5);
    h_nbjets_ele_matched = new TH1F("h_nbjets_ele_matched", ";N bjets ele matched", 5, 0, 5);
    initialize_tools();
}
//////////////////////////////////////////////////////////////////////////////
void FTAGLooper::initialize_tools()
{
    string fn = "FTAGLooper::initialize_tools";
    cout << endl;
    cout << fn << "   Initializing ASG tools..." << endl;
    cout << endl;

    // jet calibration tool
    string name = "FTAGLooperJets_AntiKt4EMTopo";
    string jetname = "AntiKt4EMTopo";
    string jetcol = "AntiKt4EMTopoJets";
    string calib_area = "00-04-81";
    string JES_config_file = "JES_MC16Recommendation_28Nov2017.config";
    std::string calibseq = "JetArea_Residual_EtaJES_GSC";
    bool is_data = false;

    m_jetCalibTool.setTypeAndName("JetCalibrationTool/" + name);
    CHECK( m_jetCalibTool.setProperty("JetCollection", jetname) );
    CHECK( m_jetCalibTool.setProperty("ConfigFile", JES_config_file) );
    CHECK( m_jetCalibTool.setProperty("CalibSequence", calibseq) );
    CHECK( m_jetCalibTool.setProperty("CalibArea", calib_area) );
    CHECK( m_jetCalibTool.setProperty("IsData", is_data) );
    CHECK( m_jetCalibTool.retrieve() );

    // jet cleaning tool
    name = "FTAGLooper_JetCleaningTool";
    m_jetCleaningTool.setTypeAndName("JetCleaningTool/"+name);
    CHECK( m_jetCleaningTool.setProperty("CutLevel", "LooseBad") );
    CHECK( m_jetCleaningTool.retrieve() );

    // jet jvt tool
    name = "FTAGLooper_JetVertexTaggerTool";
    m_jetJvtUpdateTool.setTypeAndName("JetVertexTaggerTool/"+name);
    CHECK( m_jetJvtUpdateTool.setProperty("JVTFileName", "JetMomentTools/JVTlikelihood_20140805.root") );
    CHECK( m_jetJvtUpdateTool.retrieve() );

    // electrons
    name = "FTAGLooper_EleLH_LooseLLH";
    string wp = "LooseLHElectron";
    m_elecSelLikelihood.setTypeAndName("AsgElectronLikelihoodTool/"+name);
    CHECK( m_elecSelLikelihood.setProperty("WorkingPoint", wp)); 
    CHECK( m_elecSelLikelihood.retrieve() );

    // electron calibration
    cout << fn << "    WARNING ASSUMING SAMPLE IS NOT ATLFAST" << endl;
    m_egammaCalibTool.setTypeAndName("CP::EgammaCalibrationAndSmearingTool/EgammaCalibrationAndSmearingTool");
    CHECK( m_egammaCalibTool.setProperty("ESModel", "es2017_R21_PRE") ); //used for analysis using data processed with 21.0
    CHECK( m_egammaCalibTool.setProperty("decorrelationModel", "1NP_v1") );
    CHECK( m_egammaCalibTool.setProperty("useAFII", 0) );
    CHECK( m_egammaCalibTool.setProperty("randomRunNumber", 123456) );
    CHECK( m_egammaCalibTool.retrieve() );

    // isolation correction
    cout << fn << "   WARNING ASSUMING SAMPLE IS MC" << endl;
    m_isoCorrTool.setTypeAndName("CP::IsolationCorrectionTool/IsoCorrTool");
    CHECK( m_isoCorrTool.setProperty( "IsMC", false) );
    CHECK( m_isoCorrTool.setProperty( "AFII_corr", false) );
    CHECK( m_isoCorrTool.retrieve() );

    cout << endl;
    cout << fn << "   Done initializing ASG tools!" << endl;
    cout << endl;
}
//////////////////////////////////////////////////////////////////////////////
bool FTAGLooper::get_metadata()
{
    string fn = "FTAGLooper::get_metadata";
    const xAOD::EventInfo* ei = 0;
    RETURN_CHECK(GetName(), event()->retrieve(ei, "EventInfo") );
    int dsid = ei->mcChannelNumber();
    cout << fn << "    DSID of sample retrieved: " << dsid << endl;
    m_dsid = dsid;

    return true;
}
//////////////////////////////////////////////////////////////////////////////
void FTAGLooper::initialize_branches(TTree* tree)
{

    m_event_number = -1;
    m_mc_channel = -1;
    m_mc_weight = 1;
    m_avg_mu = 0;
    m_avg_mu_actual = 0;
    m_pv_x = 0;
    m_pv_y = 0;
    m_pv_z = 0;
    m_truth_pv_x = 0;
    m_truth_pv_y = 0;
    m_truth_pv_z = 0;

    m_jf_PV_x = -99.;
    m_jf_PV_y = -99.;
    m_jf_PV_z = -99.;
    m_jf_PV_x_em = -99.;
    m_jf_PV_y_em = -99.;
    m_jf_PV_z_em = -99.;
    m_jf_PV_x_tm = -99.;
    m_jf_PV_y_tm = -99.;
    m_jf_PV_z_tm = -99.;
    

#define INIT_INT_BRANCH( name ) \
    do { \
        name = -99; \
        (name##_em) = -99.; \
        (name##_tm) = -99.; \
    } while (0);

#define INIT_VF_BRANCH( name ) \
    do { \
         name = new vector<float>(); \
         (name##_em) = new vector<float>(); \
         (name##_tm) = new vector<float>(); \
    } while (0);

#define INIT_VVF_BRANCH( name ) \
    do { \
        name = new vector< vector< float > >(); \
        (name##_em) = new vector< vector< float > >(); \
        (name##_tm) = new vector< vector< float > >(); \
    } while (0);

#define INIT_VVI_BRANCH( name ) \
    do { \
        name = new vector< vector< int > >(); \
        (name##_em) = new vector< vector< int > >(); \
        (name##_tm) = new vector< vector< int > >(); \
    } while(0);

#define INIT_VD_BRANCH( name ) \
    do { \
         name = new vector<double>(); \
         (name##_em) = new vector<double>(); \
         (name##_tm) = new vector<double>(); \
    } while (0);

#define INIT_VI_BRANCH( name ) \
    do { \
        name = new vector<int>(); \
        (name##_em) = new vector<int>(); \
        (name##_tm) = new vector<int>(); \
    } while (0);


    INIT_VF_BRANCH(v_jet_pt)
    INIT_VF_BRANCH(v_jet_eta)
    INIT_VF_BRANCH(v_jet_phi)
    INIT_VF_BRANCH(v_jet_E)
    INIT_VF_BRANCH(v_jet_JVT)
    INIT_VF_BRANCH(v_jet_m)
    INIT_VF_BRANCH(v_jet_nConst)
    INIT_VF_BRANCH(v_jet_dRiso)
    INIT_VF_BRANCH(v_jet_dRminToB)
    INIT_VF_BRANCH(v_jet_dRminToC)
    INIT_VF_BRANCH(v_jet_dRminToT)

    INIT_VI_BRANCH(v_sv1_NVtx)
    INIT_VF_BRANCH(v_sv1_NGTinSvx)
    INIT_VF_BRANCH(v_sv1_N2Tpair)
    INIT_VF_BRANCH(v_sv1_masssvx)
    INIT_VF_BRANCH(v_sv1_efracsvx)
    INIT_VF_BRANCH(v_sv1_normdist)
    INIT_VF_BRANCH(v_sv1_significance3d)
    INIT_VF_BRANCH(v_sv1_deltaR)
    INIT_VF_BRANCH(v_sv1_Lxy)
    INIT_VF_BRANCH(v_sv1_L3d)

    INIT_VD_BRANCH(v_jet_dl1_pb)
    INIT_VD_BRANCH(v_jet_dl1_pc)
    INIT_VD_BRANCH(v_jet_dl1_pu)
    INIT_VD_BRANCH(v_jet_dl1)
    INIT_VD_BRANCH(v_jet_mv2c10)

    INIT_VVI_BRANCH(v_jf_jet_trk_vertex)
    INIT_VF_BRANCH(v_jf_jet_pb)
    INIT_VF_BRANCH(v_jf_jet_pc)
    INIT_VF_BRANCH(v_jf_jet_pu)
    INIT_VF_BRANCH(v_jf_jet_llr)
    INIT_VF_BRANCH(v_jf_jet_m)
    INIT_VF_BRANCH(v_jf_jet_efc)
    INIT_VF_BRANCH(v_jf_jet_deta)
    INIT_VF_BRANCH(v_jf_jet_dphi)
    INIT_VF_BRANCH(v_jf_jet_dR)
    INIT_VF_BRANCH(v_jf_jet_ntrkAtVx)
    INIT_VF_BRANCH(v_jf_jet_nvtx)
    INIT_VF_BRANCH(v_jf_jet_sig3d)
    INIT_VF_BRANCH(v_jf_jet_nvtx1t)
    INIT_VF_BRANCH(v_jf_jet_n2t)
    INIT_VF_BRANCH(v_jf_jet_VTXsize)

    INIT_VF_BRANCH(v_jet_ip2d_pb)
    INIT_VF_BRANCH(v_jet_ip2d_pc)
    INIT_VF_BRANCH(v_jet_ip2d_pu)
    INIT_VF_BRANCH(v_jet_ip2d_llr)
    INIT_VF_BRANCH(v_jet_ip3d_pb)
    INIT_VF_BRANCH(v_jet_ip3d_pc)
    INIT_VF_BRANCH(v_jet_ip3d_pu)
    INIT_VF_BRANCH(v_jet_ip3d_llr)

    INIT_VVF_BRANCH(v_jf_jet_vtx_chi2)
    INIT_VVF_BRANCH(v_jf_jet_vtx_ndf)
    INIT_VVF_BRANCH(v_jf_jet_vtx_ntrk)
    INIT_VVF_BRANCH(v_jf_jet_vtx_L3d)
    INIT_VVF_BRANCH(v_jf_jet_vtx_sig3d)
    INIT_VVF_BRANCH(v_jf_jet_vtx_x)
    INIT_VVF_BRANCH(v_jf_jet_vtx_x_err)
    INIT_VVF_BRANCH(v_jf_jet_vtx_y)
    INIT_VVF_BRANCH(v_jf_jet_vtx_y_err)
    INIT_VVF_BRANCH(v_jf_jet_vtx_z)
    INIT_VVF_BRANCH(v_jf_jet_vtx_z_err)

    INIT_VF_BRANCH(v_jf_nTrk_vtx1)
    INIT_VF_BRANCH(v_jf_mass_first_vtx)
    INIT_VF_BRANCH(v_jf_e_first_vtx)
    INIT_VF_BRANCH(v_jf_e_frac_vtx1)
    INIT_VF_BRANCH(v_jf_closestVtx_L3D)
    INIT_VF_BRANCH(v_jf_JF_Lxy1)
    INIT_VF_BRANCH(v_jf_vtx1_MaxTrkRapidity_jf_path)
    INIT_VF_BRANCH(v_jf_vtx1_AvgTrkRapidity_jf_path)
    INIT_VF_BRANCH(v_jf_vtx1_MinTrkRapidity_jf_path)

    INIT_VF_BRANCH(v_jf_nTrk_vtx2)
    INIT_VF_BRANCH(v_jf_mass_second_vtx)
    INIT_VF_BRANCH(v_jf_e_second_vtx)
    INIT_VF_BRANCH(v_jf_e_frac_vtx2)
    INIT_VF_BRANCH(v_jf_second_closestVtx_L3D)
    INIT_VF_BRANCH(v_jf_JF_Lxy2)
    INIT_VF_BRANCH(v_jf_vtx2_MaxTrkRapidity_jf_path)
    INIT_VF_BRANCH(v_jf_vtx2_AvgTrkRapidity_jf_path)
    INIT_VF_BRANCH(v_jf_vtx2_MinTrkRapidity_jf_path)

    INIT_VF_BRANCH(v_jf_MaxTrkRapidity_jf_path)
    INIT_VF_BRANCH(v_jf_MinTrkRapidity_jf_path)
    INIT_VF_BRANCH(v_jf_AvgTrkRapidity_jf_path)


    // event quantities

    tree->Branch("event_number", &m_event_number);
    tree->Branch("mc_channel", &m_mc_channel);
    tree->Branch("mc_weight", &m_mc_weight);
    tree->Branch("avg_mu", &m_avg_mu);
    tree->Branch("avg_mu_actual", &m_avg_mu_actual);
    tree->Branch("pv_x", &m_pv_x);
    tree->Branch("pv_y", &m_pv_y);
    tree->Branch("pv_z", &m_pv_z);
    tree->Branch("truth_pv_x", &m_truth_pv_x);
    tree->Branch("truth_pv_y", &m_truth_pv_y);
    tree->Branch("truth_pv_z", &m_truth_pv_z);
    

    // jet properties

    INIT_INT_BRANCH(n_jet);
#define BRANCH( name ) \
    do { \
        string obname = #name; \
        string bname = obname; \
        tree->Branch( bname.c_str(), &name); \
        bname = (obname + "_em"); \
        tree->Branch( bname.c_str(), &name##_em); \
        bname = (obname + "_tm"); \
        tree->Branch( bname.c_str(), &name##_tm); \
    } while (0);


    BRANCH(n_jet);
    BRANCH(v_jet_pt);
    BRANCH(v_jet_eta);
    BRANCH(v_jet_phi);
    BRANCH(v_jet_E)
    BRANCH(v_jet_JVT)
    BRANCH(v_jet_m)
    BRANCH(v_jet_nConst)
    BRANCH(v_jet_dRiso)
    BRANCH(v_jet_dRminToB)
    BRANCH(v_jet_dRminToC)
    BRANCH(v_jet_dRminToT)

    BRANCH(v_jet_dl1_pb)
    BRANCH(v_jet_dl1_pc)
    BRANCH(v_jet_dl1_pu)
    BRANCH(v_jet_dl1)
    BRANCH(v_jet_mv2c10)

    BRANCH(v_sv1_NVtx)
    BRANCH(v_sv1_NGTinSvx)
    BRANCH(v_sv1_N2Tpair)
    BRANCH(v_sv1_masssvx)
    BRANCH(v_sv1_efracsvx)
    BRANCH(v_sv1_normdist)
    BRANCH(v_sv1_significance3d)
    BRANCH(v_sv1_deltaR)
    BRANCH(v_sv1_Lxy)
    BRANCH(v_sv1_L3d)

    BRANCH(v_jf_jet_trk_vertex)
    BRANCH(v_jf_jet_pb)
    BRANCH(v_jf_jet_pc)
    BRANCH(v_jf_jet_pu)
    BRANCH(v_jf_jet_llr)
    BRANCH(v_jf_jet_m)
    BRANCH(v_jf_jet_efc)
    BRANCH(v_jf_jet_deta)
    BRANCH(v_jf_jet_dphi)
    BRANCH(v_jf_jet_dR)
    BRANCH(v_jf_jet_ntrkAtVx)
    BRANCH(v_jf_jet_nvtx)
    BRANCH(v_jf_jet_sig3d)
    BRANCH(v_jf_jet_nvtx1t)
    BRANCH(v_jf_jet_n2t)
    BRANCH(v_jf_jet_VTXsize)

    BRANCH(v_jf_jet_vtx_chi2)
    BRANCH(v_jf_jet_vtx_ndf)
    BRANCH(v_jf_jet_vtx_ntrk)
    BRANCH(v_jf_jet_vtx_L3d)
    BRANCH(v_jf_jet_vtx_sig3d)
    BRANCH(v_jf_jet_vtx_x)
    BRANCH(v_jf_jet_vtx_x_err)
    BRANCH(v_jf_jet_vtx_y)
    BRANCH(v_jf_jet_vtx_y_err)
    BRANCH(v_jf_jet_vtx_z)
    BRANCH(v_jf_jet_vtx_z_err)

    BRANCH(v_jf_nTrk_vtx1)
    BRANCH(v_jf_mass_first_vtx)
    BRANCH(v_jf_e_first_vtx)
    BRANCH(v_jf_e_frac_vtx1)
    BRANCH(v_jf_closestVtx_L3D)
    BRANCH(v_jf_JF_Lxy1)
    BRANCH(v_jf_vtx1_MaxTrkRapidity_jf_path)
    BRANCH(v_jf_vtx1_AvgTrkRapidity_jf_path)
    BRANCH(v_jf_vtx1_MinTrkRapidity_jf_path)

    BRANCH(v_jf_nTrk_vtx2)
    BRANCH(v_jf_mass_second_vtx)
    BRANCH(v_jf_e_second_vtx)
    BRANCH(v_jf_e_frac_vtx2)
    BRANCH(v_jf_second_closestVtx_L3D)
    BRANCH(v_jf_JF_Lxy2)
    BRANCH(v_jf_vtx2_MaxTrkRapidity_jf_path)
    BRANCH(v_jf_vtx2_AvgTrkRapidity_jf_path)
    BRANCH(v_jf_vtx2_MinTrkRapidity_jf_path)

    BRANCH(v_jf_MaxTrkRapidity_jf_path)
    BRANCH(v_jf_MinTrkRapidity_jf_path)
    BRANCH(v_jf_AvgTrkRapidity_jf_path)

    BRANCH(v_jet_ip2d_pb)
    BRANCH(v_jet_ip2d_pc)
    BRANCH(v_jet_ip2d_pu)
    BRANCH(v_jet_ip2d_llr)
    BRANCH(v_jet_ip3d_pb)
    BRANCH(v_jet_ip3d_pc)
    BRANCH(v_jet_ip3d_pu)
    BRANCH(v_jet_ip3d_llr)

#undef INIT_VF_BRANCH
#undef INIT_VD_BRANCH
#undef INIT_INT_BRANCH
#undef BRANCH 
}
//////////////////////////////////////////////////////////////////////////////
void FTAGLooper::setup_output_tree()
{
    string fn = "FTAGLoooper::setup_output_tree";
    cout << fn << endl;


    stringstream ofn;
    ofn << "ftag_output_" << dsid();
    if(suffix()!="") {
        ofn << "_" << suffix();
    }
    ofn << ".root";
    m_output_tree_file = new TFile(ofn.str().c_str(), "RECREATE");
    m_tree = new TTree("ftag", "ftag");

    // BRANCHES
    initialize_branches(m_tree);

    return;
}
//////////////////////////////////////////////////////////////////////////////
Bool_t FTAGLooper::Notify()
{
    cout << "FTAGLooper::Notify" << endl;
    return true;
}
//////////////////////////////////////////////////////////////////////////////
void FTAGLooper::Begin(TTree* /*tree*/)
{
    cout << "FTAGLooper::Begin" << endl;
}
//////////////////////////////////////////////////////////////////////////////
void FTAGLooper::SlaveBegin(TTree* /*tree*/)
{
    cout << "FTAGLooper::SlaveBegin" << endl;
}
//////////////////////////////////////////////////////////////////////////////
void FTAGLooper::Terminate()
{
    cout << "FTAGLooper::Terminate" << endl;

    
    TFile* outfile = new TFile("ftag_output.root", "RECREATE");
    outfile->cd();
    h_drbe->Write();
    h_nbjets->Write();
    h_nbjets_truth_matched->Write();
    h_nbjets_ele_matched->Write();
    outfile->Write();
    outfile->Close();

    save_output();
}
//////////////////////////////////////////////////////////////////////////////
void FTAGLooper::save_output()
{
    string fn = "FTAGLooper::save_output";
    cout << fn << endl;

    if(!m_output_tree_file) {
        cout << fn << "   Output file is null, not saving output" << endl;
        return; 
    }
    //m_output_tree_file = m_output_tree->GetCurrentFile();
    //if(m_output_tree_file) {
    //    TDirectory* current_directory = gROOT->CurrentDirectory();
    //    m_output_tree_file->cd();
    //    TNamed n("inputSample", m_input_samplename.c_str());
    //    n.Write();
    //    current_directory->cd();
    //}
    m_output_tree_file->cd();
    m_tree->Write();
    m_output_tree_file->Write(0, TObject::kOverwrite);
    cout << fn << "   Output tree saved to " << m_output_tree_file->GetName() << endl;
    m_output_tree_file->Close();
}
//////////////////////////////////////////////////////////////////////////////
Bool_t FTAGLooper::Process(Long64_t /*entry*/)
{
    string fn = "FTAGLooper::Process";

    static Long64_t chain_entry = -1;
    chain_entry++;
    event()->getEntry(chain_entry);

    const xAOD::EventInfo* ei = 0;
    RETURN_CHECK(GetName(), event()->retrieve(ei, "EventInfo"));
    int event_number = ei->eventNumber();

    if(dbg() || (chain_entry % 5000 == 0)) {
        cout << "FTAGLoopper::Process    **** Processing event " << chain_entry
            << "  event " << event_number << " ****" << endl;
    }


    if(!m_output_setup) {
        if(!get_metadata()) { exit(1); }
        setup_output_tree();
        m_output_setup = true;
    }

    m_event_number = event_number;
    m_mc_channel = ei->mcChannelNumber();
    #warning not applying MC event weights
    m_mc_weight = 1;
    m_avg_mu = ei->averageInteractionsPerCrossing();
    m_avg_mu_actual = ei->actualInteractionsPerCrossing();
    const xAOD::Vertex* pv = FTAGLooper::GetPrimVtx();
    m_pv_x = pv->x();
    m_pv_y = pv->y();
    m_pv_z = pv->z();

    const xAOD::VertexContainer* primary_vertices = 0;
    CHECK( event()->retrieve(primary_vertices, "PrimaryVertices") );
    
    // jets
    const xAOD::JetContainer* xjets = 0;
    CHECK( event()->retrieve(xjets, "AntiKt4EMTopoJets") );
    vector<xAOD::Jet*> jets;
    vector<xAOD::Jet*> bjets;
    vector<xAOD::Jet*> bjets_truth_matched;
    //cout << "---------------------------"  << endl;
    for(const auto* jet : *xjets) {
        xAOD::Jet* calib_jet = nullptr;
        //float pt_before = jet->pt() * mev2gev;
        //float eta_before = jet->eta();
        m_jetCalibTool->calibratedCopy(*jet, calib_jet);
        //float pt_after = calib_jet->pt() * mev2gev;
        //float eta_after = calib_jet->eta();
        //cout << "jet pt before calibration = (" << pt_before << ", " << eta_before << ")  after = (" << pt_after << ", " << eta_after << ")" << endl;
        float jvt = m_jetJvtUpdateTool->updateJvt(*calib_jet);
        dec_jvt(*calib_jet) = jvt;

        if(!m_jetCleaningTool->keep(*calib_jet)) continue;

    //    const xAOD::BTagging* bjet = calib_jet->btagging();
    //std::vector<ElementLink<xAOD::BTagVertexContainer> > jfvertices;
    //try {
    //  jfvertices =  bjet->auxdata<std::vector<ElementLink<xAOD::BTagVertexContainer> > >("JetFitter_JFvertices");
    //} catch (...) {};
    //int tmpNvtx = 0;
    //int tmpNvtx1t = 0;

    //bjet->taggerInfo(tmpNvtx, xAOD::JetFitter_nVTX);
    //bjet->taggerInfo(tmpNvtx1t, xAOD::JetFitter_nSingleTracks); // 1 track vertices

    //if (jfvertices.size()>0 && jfvertices[0].isValid() && (tmpNvtx > 0 || tmpNvtx1t > 0) ){
    //    float jfm = -1.0;
    //    float jfefc = -1.0;
    //    bjet->taggerInfo(jfm, xAOD::JetFitter_mass);
    //    bjet->taggerInfo(jfefc, xAOD::JetFitter_energyFraction);
    //    cout << "JF jfm = " << jfm << ",  jfefc = " << jfefc << endl;
    //}



        

        //const auto sv1_vertices = acc_sv1_vertices(*bjet);
        //float sv1_efracx = acc_sv1_efracsvx(*bjet);
        //
        //cout << "sv1_vertices size = " << sv1_vertices.size() << "  efracsvx = " << sv1_efracx << endl;

        jets.push_back(calib_jet);
        if(isBJet(calib_jet)) {
            bjets.push_back(calib_jet);
            int truth_label = -1;
            calib_jet->getAttribute("HadronConeExclTruthLabelID", truth_label);
            if(truth_label == 5)
                bjets_truth_matched.push_back(calib_jet);

        }

    }

    const xAOD::JetContainer* truth_jets = 0;
    CHECK( event()->retrieve(truth_jets, "AntiKt4TruthJets") );

    // electrons
    const xAOD::ElectronContainer* xelectrons = 0;
    CHECK( event()->retrieve(xelectrons, "Electrons") );

    vector<const xAOD::Electron*> electrons;
    for(const auto* electron : *xelectrons) {
        //xAOD::Electron* electron = ;


        //unsigned char el_nPixHits(0), el_nSCTHits(0);
        //input.trackParticle()->summaryValue(el_nPixHits, xAOD::numberOfPixelHits);
        //input.trackParticle()->summaryValue(el_nSCTHits, xAOD::numberOfSCTHits);
        
        if(!electron->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON)) continue;

        // for these studies we dont care about LH since FTAG2 requires >=2 
        // LHLoose electrons
        // https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/DerivationFramework/DerivationFrameworkFlavourTag/trunk/share/FTAG2.py
        //if(!m_elecSelLikelihood->accept(electron)) continue;

        if (fabs(electron->caloCluster()->eta()) >= 2.47) continue;

        bool apply_crack_veto = false;
        if (apply_crack_veto){
          if  ( fabs( electron->caloCluster()->etaBE(2) ) >1.37 &&  fabs( electron->caloCluster()->etaBE(2) ) <1.52) {
            continue;
          }
        }

        //if( m_egammaCalibTool->correctedCopy(*electron, electron) != CP::CorrectionCode::Ok )
        //    cout << fn << "    WARNING Failed to apply calibration to electron!" << endl;

        //if ( m_isoCorrTool->applyCorrection(*electron)  != CP::CorrectionCode::Ok)
        //    cout << fn << "    WARNING Failed to apply isolation correction to electron!" << endl;


        const xAOD::Vertex* pv = FTAGLooper::GetPrimVtx();
        const xAOD::TrackParticle* track = electron->trackParticle();
        double primvertex_z = pv ? pv->z() : 0;
        double el_z0 = track->z0() + track->vz() - primvertex_z;
        dec_z0sinTheta(*electron) = el_z0 * TMath::Sin(electron->p4().Theta());
        try{
          dec_d0sig(*electron) = xAOD::TrackingHelpers::d0significance( track , ei->beamPosSigmaX(), ei->beamPosSigmaY(), ei->beamPosSigmaXY() );
        }
        catch(...){
          float d0sigError = -99.;
          cout << fn << "    WARNING Exception catched from d0significance() calculation. Setting dummy decoration d0sig=" << d0sigError << endl;;
          dec_d0sig(*electron) = d0sigError;
        }


        if( !(electron->pt()*mev2gev >= 10.))  continue;

        for(const auto jet : jets) {
            TLorentzVector jlv;
            TLorentzVector elv;
            elv.SetPtEtaPhiM(electron->pt()*mev2gev, electron->eta(), electron->phi(), electron->m()*mev2gev);
            jlv.SetPtEtaPhiM(jet->pt()*mev2gev, jet->eta(), jet->phi(), jet->m()*mev2gev);
            double mv2_weight(0.);
            if(!jet->btagging()->MVx_discriminant("MV2c10", mv2_weight)) {
                cout << fn << "    WARNING Failed to retrieve MV2c10 weight" << endl;
                continue;
            }
            float dr = elv.DeltaR(jlv);
            if(mv2_weight > 0.11) {
                if(dr<0.001)
                    h_drbe->Fill(dr);
            }
        }

        electrons.push_back(electron);

    }

    vector<xAOD::Jet*> bjets_ele_matched;
    vector<int> matched_reco_ele_idx;
    vector<float> min_dr_bjet_ele;
    for(const auto & bjet : bjets) {
        bool has_ele_match = false;
        TLorentzVector jlv;
        jlv.SetPtEtaPhiM(bjet->pt()*mev2gev, bjet->eta(), bjet->phi(), bjet->m()*mev2gev);
        size_t ele_match_idx = 0;
        float min_dr = 999.;
        for(size_t ie = 0; ie < electrons.size(); ie++) {
            TLorentzVector elv;
            elv.SetPtEtaPhiM(electrons.at(ie)->pt()*mev2gev, electrons.at(ie)->eta(),
                    electrons.at(ie)->phi(), electrons.at(ie)->m()*mev2gev);
            float dr = jlv.DeltaR(elv);
            if(dr < min_dr) { min_dr = dr; ele_match_idx = ie; }
            if(jet_matches_electron(bjet, electrons.at(ie), 0.01, 0.7)) {
                bjets_ele_matched.push_back(bjet);
                has_ele_match = true;
            }
        } // ele
        if(has_ele_match) {
            matched_reco_ele_idx.push_back(ele_match_idx);
        }
        min_dr_bjet_ele.push_back(min_dr);
    } // bjet

    h_nbjets->Fill(bjets.size());
    h_nbjets_truth_matched->Fill(bjets_truth_matched.size());
    h_nbjets_ele_matched->Fill(bjets_ele_matched.size());

    // truth particles
    vector<const xAOD::TruthParticle*> partonB;
    vector<const xAOD::TruthParticle*> partonC;
    vector<const xAOD::TruthParticle*> partonT;
    const xAOD::TruthEventContainer* xTruth = nullptr;
    CHECK( event()->retrieve(xTruth, "TruthEvents") );
    for(const auto * truth : *xTruth) {

        const xAOD::TruthVertex *truthVertex = truth->signalProcessVertex();
        if(truthVertex) {
            m_truth_pv_x = truthVertex->x();
            m_truth_pv_y = truthVertex->y();
            m_truth_pv_z = truthVertex->z();
        }

        for(unsigned int i = 0; i < truth->nTruthParticles(); i++) {
            const xAOD::TruthParticle* particle = truth->truthParticle(i);
            if(!particle) continue;
            if(particle->pt() > 3e3) {
                if(particle->absPdgId() == 15) partonT.push_back(particle);
                if(particle->isCharmHadron()) partonC.push_back(particle);
                if(particle->isBottomHadron()) partonB.push_back(particle);
            }

            //if( (particle->pt()*mev2gev) < 10. ) continue;
            //if( (particle->status() != 1) ) continue;
            //if( (particle->barcode() > 2e5) ) continue;

            //if( !(particle->absPdgId()==11 || particle->absPdgId()==13) ) continue;
            //bool isFromWZ(particle)

        } // i
    }


    // fill the tree
    fill_jet_properties(bjets, bjets_ele_matched, bjets_truth_matched,
            partonB, partonC, partonT);

    m_tree->Fill();

    return true; 
}
//////////////////////////////////////////////////////////////////////////////
const xAOD::Vertex* FTAGLooper::GetPrimVtx() {
  const xAOD::VertexContainer* vertices(0);
  if ( event()->retrieve( vertices, "PrimaryVertices" ).isSuccess() ) {
    for ( const auto& vx : *vertices ) {
      if (vx->vertexType() == xAOD::VxType::PriVtx) {
        return vx;
      }
    }
  }
  else {
        cout << "FTAGLooper::GetPrimVtx    WARNING Did not find primary vertex, returning NULL" << endl;
  }
  return NULL;
}
//////////////////////////////////////////////////////////////////////////////
bool FTAGLooper::passJvt(const xAOD::Jet* jet)
{
    return ( (acc_jvt(*jet) > 0.59) ||
                (fabs(jet->eta()) > 2.4) ||
                ((jet->pt() * mev2gev) > 60.) );
}
//////////////////////////////////////////////////////////////////////////////
bool FTAGLooper::isBJet(const xAOD::Jet* jet, int wp, bool use_dl1)
{
    string fn = "FTAGLooper::isBJet";

    bool pass_pt = (jet->pt()*mev2gev) > 20.;
    bool pass_eta = fabs(jet->eta()) < 2.4;
    bool pass_jvt = FTAGLooper::passJvt(jet);

    double mv2_weight(0.);
    if(!jet->btagging()->MVx_discriminant("MV2c10", mv2_weight)) {
        cout << fn << "    WARNING Failed to retrieve MV2c10 weight (jet (pt,eta) = (" << jet->pt()*mev2gev << ", " << jet->eta() << "))" << endl;
        return false;
    }
    double dl1_weight(0.);
    dl1_weight = get_dl1_tagger_weight(jet)[0];

    bool pass_tagger = false;
    if(wp == 85 && !use_dl1) {
        pass_tagger = (mv2_weight > mv2_85_cut);
    }
    else if(wp == 77 && !use_dl1) {
        pass_tagger = (mv2_weight > mv2_77_cut);
    }
    else if(wp == 85 && use_dl1) {
        pass_tagger = (dl1_weight > dl1_85_cut);
    }
    else if(wp == 77 && use_dl1) {
        pass_tagger = (dl1_weight > dl1_77_cut);
    }
    else {
        cout << fn << "   ERROR Shouldn't be able to get here!" << endl;
        exit(1);
    }

    return (pass_pt && pass_eta && pass_jvt && pass_tagger);
}
//////////////////////////////////////////////////////////////////////////////
vector<float> FTAGLooper::get_dl1_tagger_weight(const xAOD::Jet* jet, float fraction)
{
    string fn = "FTAGLooper::get_dl1_tagger_weight";
    double dl1_pb(-10.);
    double dl1_pc(-10.);
    double dl1_pu(-10.);

    vector<float> out { -10., -10., -10., -10. };

    const xAOD::BTagging* btag = jet->btagging();

    if((!btag)) {
        cout << fn << "    ERROR Failed to get BTagging information!" << endl;
    }

    string tagger = "DL1";
    bool pb_ok = btag->pb(tagger, dl1_pb);
    bool pc_ok = btag->pc(tagger, dl1_pc);
    bool pu_ok = btag->pu(tagger, dl1_pu);

    if(!(pb_ok || pc_ok || pu_ok)) {
        cout << fn << "    ERROR Failed to get " << tagger << " weights!" << endl;
    }

    bool valid_input = (!std::isnan(dl1_pu) && dl1_pb>0 && dl1_pc>0 && dl1_pu>0);

    if(!valid_input) {
        cout << fn << "    ERROR Invalid inputs for " << tagger << "weights!" << endl;
        return out;;
    }

    float tag_weight = -100.;
    tag_weight = log( dl1_pb / (fraction * dl1_pc + (1. - fraction) * dl1_pu) );
    out[0] = tag_weight;
    out[1] = dl1_pb;
    out[2] = dl1_pc;
    out[3] = dl1_pu;
    return out;
}
//////////////////////////////////////////////////////////////////////////////
bool FTAGLooper::jet_matches_electron(const xAOD::Jet* jet,
        const xAOD::Electron* electron, float dr_cut, float pt_ratio_cut)
{
    TLorentzVector jlv;
    TLorentzVector elv;
    jlv.SetPtEtaPhiM(jet->pt()*mev2gev, jet->eta(), jet->phi(), jet->m()*mev2gev);
    elv.SetPtEtaPhiM(electron->pt()*mev2gev, electron->eta(), electron->phi(), electron->m()*mev2gev);

    float dr = jlv.DeltaR(elv);
    bool pass_dr = (dr < dr_cut);

    float pt_ratio = (elv.Pt() / jlv.Pt());
    bool pass_ratio = (pt_ratio > pt_ratio_cut);

    if(pass_dr && pass_ratio) return true;
    else { return false; }
}
//////////////////////////////////////////////////////////////////////////////
FTAGLooper::~FTAGLooper()
{
    cout << "FTAGLooper::~FTAGLooper" << endl;
    delete m_event;
}
//////////////////////////////////////////////////////////////////////////////
void FTAGLooper::fill_jet_properties(vector<xAOD::Jet*> bjets,
        vector<xAOD::Jet*> bjets_em, vector<xAOD::Jet*> bjets_tm,
        vector<const xAOD::TruthParticle*> bhad,
        vector<const xAOD::TruthParticle*> chad,
        vector<const xAOD::TruthParticle*> tlep)
{

    m_jf_PV_x = -99.;
    m_jf_PV_y = -99.;
    m_jf_PV_z = -99.;
    m_jf_PV_x_em = -99.;
    m_jf_PV_y_em = -99.;
    m_jf_PV_z_em = -99.;
    m_jf_PV_x_tm = -99.;
    m_jf_PV_y_tm = -99.;
    m_jf_PV_z_tm = -99.;

#define CLEAR_JETPROPERTIES( type ) \
    do { \
        v_jet_pt##type->clear(); \
        v_jet_eta##type->clear(); \
        v_jet_phi##type->clear(); \
        v_jet_E##type->clear(); \
        v_jet_JVT##type->clear(); \
        v_jet_m##type->clear(); \
        v_jet_nConst##type->clear(); \
        v_jet_dRiso##type->clear(); \
        v_jet_dRminToB##type->clear(); \
        v_jet_dRminToC##type->clear(); \
        v_jet_dRminToT##type->clear(); \
        v_jet_dl1_pb##type->clear(); \
        v_jet_dl1_pc##type->clear(); \
        v_jet_dl1_pu##type->clear(); \
        v_jet_dl1##type->clear(); \
        v_jet_mv2c10##type->clear(); \
        v_sv1_NVtx##type->clear(); \
        v_sv1_NGTinSvx##type->clear(); \
        v_sv1_N2Tpair##type->clear(); \
        v_sv1_masssvx##type->clear(); \
        v_sv1_efracsvx##type->clear(); \
        v_sv1_normdist##type->clear(); \
        v_sv1_significance3d##type->clear(); \
        v_sv1_deltaR##type->clear(); \
        v_sv1_Lxy##type->clear(); \
        v_sv1_L3d##type->clear(); \
        v_jf_jet_trk_vertex##type->clear(); \
        v_jf_jet_pb##type->clear(); \
        v_jf_jet_pc##type->clear(); \
        v_jf_jet_pu##type->clear(); \
        v_jf_jet_llr##type->clear(); \
        v_jf_jet_m##type->clear(); \
        v_jf_jet_efc##type->clear(); \
        v_jf_jet_deta##type->clear(); \
        v_jf_jet_dphi##type->clear(); \
        v_jf_jet_dR##type->clear(); \
        v_jf_jet_ntrkAtVx##type->clear(); \
        v_jf_jet_nvtx##type->clear(); \
        v_jf_jet_sig3d##type->clear(); \
        v_jf_jet_nvtx1t##type->clear(); \
        v_jf_jet_n2t##type->clear(); \
        v_jf_jet_VTXsize##type->clear(); \
        v_jf_jet_vtx_chi2##type->clear(); \
        v_jf_jet_vtx_ndf##type->clear(); \
        v_jf_jet_vtx_ntrk##type->clear(); \
        v_jf_jet_vtx_L3d##type->clear(); \
        v_jf_jet_vtx_sig3d##type->clear(); \
        v_jf_jet_vtx_x##type->clear(); \
        v_jf_jet_vtx_x_err##type->clear(); \
        v_jf_jet_vtx_y##type->clear(); \
        v_jf_jet_vtx_y_err##type->clear(); \
        v_jf_jet_vtx_z##type->clear(); \
        v_jf_jet_vtx_z_err##type->clear(); \
        v_jf_nTrk_vtx1##type->clear(); \
        v_jf_mass_first_vtx##type->clear(); \
        v_jf_e_first_vtx##type->clear(); \
        v_jf_e_frac_vtx1##type->clear(); \
        v_jf_closestVtx_L3D##type->clear(); \
        v_jf_JF_Lxy1##type->clear(); \
        v_jf_vtx1_MaxTrkRapidity_jf_path##type->clear(); \
        v_jf_vtx1_AvgTrkRapidity_jf_path##type->clear(); \
        v_jf_vtx1_MinTrkRapidity_jf_path##type->clear(); \
        v_jf_nTrk_vtx2##type->clear(); \
        v_jf_mass_second_vtx##type->clear(); \
        v_jf_e_second_vtx##type->clear(); \
        v_jf_e_frac_vtx2##type->clear(); \
        v_jf_second_closestVtx_L3D##type->clear(); \
        v_jf_JF_Lxy2##type->clear(); \
        v_jf_vtx2_MaxTrkRapidity_jf_path##type->clear(); \
        v_jf_vtx2_AvgTrkRapidity_jf_path##type->clear(); \
        v_jf_vtx2_MinTrkRapidity_jf_path##type->clear(); \
        v_jf_MaxTrkRapidity_jf_path##type->clear(); \
        v_jf_AvgTrkRapidity_jf_path##type->clear(); \
        v_jf_MinTrkRapidity_jf_path##type->clear(); \
        v_jet_ip2d_pb##type->clear(); \
        v_jet_ip2d_pc##type->clear(); \
        v_jet_ip2d_pu##type->clear(); \
        v_jet_ip2d_llr##type->clear(); \
        v_jet_ip3d_pb##type->clear(); \
        v_jet_ip3d_pc##type->clear(); \
        v_jet_ip3d_pu##type->clear(); \
        v_jet_ip3d_llr##type->clear(); \
    } while (0);

    CLEAR_JETPROPERTIES();
    CLEAR_JETPROPERTIES(_em);
    CLEAR_JETPROPERTIES(_tm);
        

    // bjets
    n_jet = bjets.size();
    for(int ib = 0; ib < (int)bjets.size(); ib++) {
        xAOD::Jet* bjet = bjets.at(ib);
        fill_jet_property(bjet, bhad, chad, tlep, "");
    }

    n_jet_em = bjets_em.size();
    for(auto & bjet : bjets_em) {
        fill_jet_property(bjet, bhad, chad, tlep, "em");
    }

    n_jet_tm = bjets_tm.size();
    for(auto & bjet : bjets_tm) {
        fill_jet_property(bjet, bhad, chad, tlep, "tm");
    }

#undef CLEAR_JETPROPERTIES
}
//////////////////////////////////////////////////////////////////////////////
void FTAGLooper::fill_jet_property(xAOD::Jet* jet,
    vector<const xAOD::TruthParticle*> bhad,
    vector<const xAOD::TruthParticle*> chad,
    vector<const xAOD::TruthParticle*> tlep,
    string type)
{
    string fn = "FTAGLooper::fill_jet_property";

    float pt = jet->pt()*mev2gev;
    float eta = jet->eta();
    float phi = jet->phi();
    float m = jet->m()*mev2gev;
    float jvt = acc_jvt(*jet);
    float e = jet->e()*mev2gev;
    float nConst = jet->numConstituents();

    float min_dR_b = 10.;
    float min_dR_c = 10.;
    float min_dR_t = 10.;

    for(size_t ip = 0; ip < bhad.size(); ip++) {
        float dr = jet->p4().DeltaR(bhad.at(ip)->p4());
        if(dr < min_dR_b) min_dR_b = dr;
    }
    for(size_t ip = 0; ip < chad.size(); ip++) {
        float dr = jet->p4().DeltaR(chad.at(ip)->p4());
        if(dr < min_dR_c) min_dR_c = dr;
    }
    for(size_t ip = 0; ip < tlep.size(); ip++) {
        float dr = jet->p4().DeltaR(tlep.at(ip)->p4());
        if(dr < min_dR_t) min_dR_t = dr;
    }

    double mv2_weight(0.);
    if(!jet->btagging()->MVx_discriminant("MV2c10", mv2_weight)) {
        cout << fn << "    WARNING Failed to retrieve MV2c10 weight (jet (pt,eta) = (" << jet->pt()*mev2gev << ", " << jet->eta() << "))" << endl;
        mv2_weight = -99.;
    }
    double dl1_weight(0.);
    vector<float> dl1_weights; // [dl1, dl1_pb, dl1_pc, dl1_pu]
    dl1_weights = get_dl1_tagger_weight(jet);
    double dl1_pb = dl1_weights[1];
    double dl1_pc = dl1_weights[2];
    double dl1_pu = dl1_weights[3];
    dl1_weight = dl1_weights[0];
    

#define FILL_JETPROPERTY( type ) \
    do { \
        v_jet_pt##type->push_back(pt); \
        v_jet_eta##type->push_back(eta); \
        v_jet_phi##type->push_back(phi); \
        v_jet_m##type->push_back(m); \
        v_jet_E##type->push_back(e); \
        v_jet_JVT##type->push_back(jvt); \
        v_jet_nConst##type->push_back(nConst); \
        v_jet_dRminToB##type->push_back(min_dR_b); \
        v_jet_dRminToC##type->push_back(min_dR_c); \
        v_jet_dRminToT##type->push_back(min_dR_t); \
        v_jet_dl1_pb##type->push_back(dl1_pb); \
        v_jet_dl1_pc##type->push_back(dl1_pc); \
        v_jet_dl1_pu##type->push_back(dl1_pu); \
        v_jet_dl1##type->push_back(dl1_weight); \
        v_jet_mv2c10##type->push_back(mv2_weight); \
    } while(0);

    if(type=="") {
        FILL_JETPROPERTY();
    }
    else if(type=="em") {
        FILL_JETPROPERTY(_em);
    }
    else if(type=="tm") {
        FILL_JETPROPERTY(_tm);
    }

    // SV1 stuff
    const xAOD::BTagging* btagging = jet->btagging();
    const auto sv1_vertices = acc_SV1_Vertices(*btagging);
    int sv1_NVtx = sv1_vertices.size();

    if(type=="") {
        v_sv1_NVtx->push_back(sv1_NVtx);
    }
    else if(type=="em") {
        v_sv1_NVtx_em->push_back(sv1_NVtx);
    }
    else if(type=="tm") {
        v_sv1_NVtx_tm->push_back(sv1_NVtx);
    }

    if(sv1_NVtx > 0) {

        float sv1_NGTinSvx = acc_SV1_NGTinSvx(*btagging);
        float sv1_N2Tpair = acc_SV1_N2Tpair(*btagging);
        float sv1_masssvx = acc_SV1_masssvx(*btagging) * mev2gev;
        float sv1_efracsvx = acc_SV1_efracsvx(*btagging);
        float sv1_normdist = acc_SV1_normdist(*btagging);
        float sv1_significance3d = acc_SV1_significance3d(*btagging);
        float sv1_deltaR = acc_SV1_deltaR(*btagging);
        float sv1_Lxy = acc_SV1_Lxy(*btagging);
        float sv1_L3d = acc_SV1_L3d(*btagging);

#define FILL_SV1PROPERTY( type ) \
    do { \
        v_sv1_NGTinSvx##type->push_back(sv1_NGTinSvx); \
        v_sv1_N2Tpair##type->push_back(sv1_N2Tpair); \
        v_sv1_masssvx##type->push_back(sv1_masssvx); \
        v_sv1_efracsvx##type->push_back(sv1_efracsvx); \
        v_sv1_normdist##type->push_back(sv1_normdist); \
        v_sv1_significance3d##type->push_back(sv1_significance3d); \
        v_sv1_deltaR##type->push_back(sv1_deltaR); \
        v_sv1_Lxy##type->push_back(sv1_Lxy); \
        v_sv1_L3d##type->push_back(sv1_L3d); \
    } while(0);

        if(type=="") {
            FILL_SV1PROPERTY();
        }
        else if(type=="em") {
            FILL_SV1PROPERTY(_em);
        }
        else if(type=="tm") {
            FILL_SV1PROPERTY(_tm);
        }
    }

    // JetFitter stuff

    float jf_jet_pb = -99.;
    float jf_jet_pc = -99.;
    float jf_jet_pu = -99.;
    float jf_jet_llr = -99.;
    float jf_jet_m = -1.;
    float jf_jet_efc = -1.;
    float jf_jet_deta = -10.;
    float jf_jet_dphi = -10.;
    float jf_jet_dR = -1.;
    float jf_jet_ntrkAtVx = -1;
    float jf_jet_nvtx = -1;
    float jf_jet_sig3d = -1;
    float jf_jet_nvtx1t = -1;
    float jf_jet_n2t = -1;
    float jf_jet_VTXsize = -1;
    

    std::vector<ElementLink<xAOD::BTagVertexContainer> > jfvertices;
    try {
        jfvertices = btagging->auxdata<std::vector<ElementLink<xAOD::BTagVertexContainer> > >("JetFitter_JFvertices");
    } catch(...) {};

    int tmpNvtx = 0;
    int tmpNvtx1t = 0;
    btagging->taggerInfo(tmpNvtx, xAOD::JetFitter_nVTX);
    btagging->taggerInfo(tmpNvtx1t, xAOD::JetFitter_nSingleTracks);

    if(jfvertices.size()>0 && jfvertices[0].isValid() && (tmpNvtx > 0 || tmpNvtx1t > 0)) {
        btagging->taggerInfo(jf_jet_m, xAOD::JetFitter_mass);
        btagging->taggerInfo(jf_jet_efc, xAOD::JetFitter_energyFraction);
        btagging->taggerInfo(jf_jet_deta, xAOD::JetFitter_deltaeta);
        btagging->taggerInfo(jf_jet_dphi, xAOD::JetFitter_deltaphi);
//        btagging->taggerInfo(jf_jet_ntrkAtVx, xAOD::BTagInfo::JetFitter_nTracksAtVtx);
        jf_jet_nvtx = tmpNvtx;
        btagging->taggerInfo(jf_jet_sig3d, xAOD::JetFitter_significance3d);
        jf_jet_nvtx1t = tmpNvtx1t;
//        btagging->taggerInfo(jf_jet_n2t, xAOD::BTagInfo::JetFitter_N2Tpair);
        jf_jet_pb = btagging->JetFitter_pb();
        jf_jet_pc = btagging->JetFitter_pc();
        jf_jet_pu = btagging->JetFitter_pu();
        jf_jet_llr = btagging->JetFitter_loglikelihoodratio();
    }

    jf_jet_dR = std::hypot(jf_jet_dphi, jf_jet_deta);
    jf_jet_VTXsize = jfvertices.size();


    vector<float> j_jf_vtx_chi2;
    vector<float> j_jf_vtx_ndf;
    vector<float> j_jf_vtx_ntrk;
    vector<float> j_jf_vtx_L3d;
    vector<float> j_jf_vtx_sig3d;
    vector<float> j_jf_vtx_sigTrans;

    vector<float> j_jf_vtx_x;
    vector<float> j_jf_vtx_x_err;
    vector<float> j_jf_vtx_y;
    vector<float> j_jf_vtx_y_err;
    vector<float> j_jf_vtx_z;
    vector<float> j_jf_vtx_z_err;

    std::vector< ElementLink< xAOD::TrackParticleContainer > > JFTracks;

    std::vector<float> fittedPosition;
    try {
        fittedPosition = btagging->auxdata<std::vector<float> >("JetFitter_fittedPosition");
    }
    catch(SG::ExcBadAuxVar& exc) {
        cout << fn << "    WARNING Missing JetFitter_fittedPosition" << endl;
    }

    std::vector<float> fittedCov;
    try {
        fittedCov = btagging->auxdata<std::vector<float> >("JetFitter_fittedCov");
    }
    catch(SG::ExcBadAuxVar& exc) {
        cout << fn << "    WARNING Missing JetFitter_fittedCov" << endl;
    }

    float jf_theta = -99.;
    float jf_theta_err = -1.;
    float jf_phi = -99.;
    float jf_phi_err = -1;
    float closestVtx_L3D = -10.;
    float second_closestVtx_L3D = -10;

    float jf_PV_x = -99.;
    float jf_PV_y = -99.;
    float jf_PV_z = -99.;

    if(fittedPosition.size()>0 && fittedCov.size()>0) {
        jf_PV_x = fittedPosition[0];
        jf_PV_y = fittedPosition[1];
        jf_PV_z = fittedPosition[2];

        jf_theta = fittedPosition[4];
        jf_theta_err = TMath::Sqrt(fittedCov[4]);
        jf_phi = fittedPosition[3];
        jf_phi_err = TMath::Sqrt(fittedCov[3]);
    } 

    TVector3 flightDir(0,0,0);
    flightDir.SetMagThetaPhi(1., jf_theta, jf_phi);

    int secondary_vertex_idx = -99;
    int tertiary_vertex_idx = -99;

    for(unsigned int jfv = 0; jfv < jfvertices.size(); jfv++) {
        if(!jfvertices.at(jfv).isValid()) continue;

        const xAOD::BTagVertex* tmpVertex = *(jfvertices.at(jfv));
        const std::vector< ElementLink<xAOD::TrackParticleContainer> > tmpVect = tmpVertex->track_links();
        JFTracks.insert(JFTracks.end(), tmpVect.begin(), tmpVect.end());


        j_jf_vtx_chi2.push_back(tmpVertex->chi2());
        j_jf_vtx_ndf.push_back(tmpVertex->NDF());
        j_jf_vtx_ntrk.push_back(tmpVect.size());

        if(jfv < fittedPosition.size()-5) {
            jf_theta = fittedPosition[4];
            jf_theta_err = TMath::Sqrt(fittedCov[4]);
            jf_phi = fittedPosition[3];
            jf_phi_err = TMath::Sqrt(fittedCov[3]);
            float jf_vtx_L3d = fittedPosition[jfv + 5]; 
            float jf_vtx_L3d_err = TMath::Sqrt(fittedCov[jfv+5]);
            float jf_vtx_Transverse_err = JF_Transverse_error(jf_vtx_L3d, jf_theta, jf_theta_err, jf_phi, jf_phi_err);

            if(jf_vtx_L3d > 0) {
                if(jf_vtx_L3d < second_closestVtx_L3D || (std::isnan(second_closestVtx_L3D) || second_closestVtx_L3D < 0)) {
                    second_closestVtx_L3D = jf_vtx_L3d;
                    tertiary_vertex_idx = jfv;

                    if(jf_vtx_L3d < closestVtx_L3D || (std::isnan(closestVtx_L3D) || closestVtx_L3D < 0)) {
                        second_closestVtx_L3D = closestVtx_L3D;
                        tertiary_vertex_idx = secondary_vertex_idx;
                        closestVtx_L3D = jf_vtx_L3d;
                        secondary_vertex_idx = jfv;
                    }
                }
        
            }
            j_jf_vtx_L3d.push_back(jf_vtx_L3d);
            j_jf_vtx_sig3d.push_back(jf_vtx_L3d_err);
            j_jf_vtx_sigTrans.push_back(jf_vtx_Transverse_err);

            vector<float> xyzresults = JF_xyz_errors(jf_vtx_L3d, jf_vtx_L3d_err,
                jf_theta, jf_theta_err,
                jf_phi, jf_phi_err,
                jf_PV_x, jf_PV_y, jf_PV_z);

            j_jf_vtx_x.push_back(xyzresults[0]);
            j_jf_vtx_x_err.push_back(xyzresults[1]);
            j_jf_vtx_y.push_back(xyzresults[2]);
            j_jf_vtx_y_err.push_back(xyzresults[3]);
            j_jf_vtx_z.push_back(xyzresults[4]);
            j_jf_vtx_z_err.push_back(xyzresults[5]);
        }
        else {
            j_jf_vtx_L3d.push_back(-999.);
            j_jf_vtx_sig3d.push_back(-999.);
            j_jf_vtx_sigTrans.push_back(-999.);

            j_jf_vtx_x.push_back    (-999.);
            j_jf_vtx_x_err.push_back(-999.);
            j_jf_vtx_y.push_back    (-999.);
            j_jf_vtx_y_err.push_back(-999.);
            j_jf_vtx_z.push_back    (-999.);
            j_jf_vtx_z_err.push_back(-999.);
        }
    } // jfv

    // tracks passing # of Pixel and SCT hits requirement
    std::vector< ElementLink< xAOD::TrackParticleContainer > > assocTracks;
    vector<const xAOD::TrackParticle*> selectedTracks;
    assocTracks = btagging->auxdata<std::vector<ElementLink<xAOD::TrackParticleContainer> > >("BTagTrackToJetAssociator");
    uint8_t getInt(8);
    for(unsigned int iT = 0; iT < assocTracks.size(); iT++) {
        if(!assocTracks.at(iT).isValid()) continue;

        const xAOD::TrackParticle* tmpTrk = *(assocTracks.at(iT));
        tmpTrk->summaryValue(getInt, xAOD::numberOfPixelHits);
        int nSi = getInt;
        tmpTrk->summaryValue(getInt, xAOD::numberOfSCTHits);
        nSi += getInt;
        if(nSi < 2) continue;
        selectedTracks.push_back(tmpTrk);
    } // iT

    // track loop variables
    vector<int> jf_jet_trk_vertex;
    TLorentzVector tracksTot4Mom(0,0,0,0);
    TLorentzVector tracksTot4Mom_firstVtx(0,0,0,0); 
    TLorentzVector tracksTot4Mom_secondVtx(0,0,0,0);

    float sumTrackRapidity = 0;
    float vtx1_sumTrackRapidity = 0;
    int vtx1_first_track = 0;
    float vtx2_sumTrackRapidity = 0;
    int vtx2_first_track = 0;
    float track_mass = 139.570; // pion mass hypothesis?
    int trkIndex = 0;

    float vtx1_MaxTrkRapidity_jf_path = -99.;
    float vtx1_AvgTrkRapidity_jf_path = -99.;
    float vtx1_MinTrkRapidity_jf_path = -99.;
    float vtx2_MaxTrkRapidity_jf_path = -99.;
    float vtx2_AvgTrkRapidity_jf_path = -99.;
    float vtx2_MinTrkRapidity_jf_path = -99.;
    float MaxTrkRapidity_jf_path = -99.;
    float AvgTrkRapidity_jf_path = -99.;
    float MinTrkRapidity_jf_path = -99.;
    int nTrk_vtx1 = -1;
    int nTrk_vtx2 = -1;
    float mass_first_vtx = -1;
    float mass_second_vtx = -1;
    float e_first_vtx = -1;
    float e_frac_vtx1 = -1;
    float e_second_vtx = -1;
    float e_frac_vtx2 = -1;
    float JF_Lxy1 = -10.;
    float JF_Lxy2 = -10.;

    for(const auto * tmpTrk : selectedTracks) {
        int myVtx = -1;
        for(unsigned int jfv = 0; jfv < jfvertices.size(); jfv++) {
            if(!jfvertices.at(jfv).isValid()) continue;
            const xAOD::BTagVertex *tmpVertex = *(jfvertices.at(jfv));
            const std::vector<ElementLink<xAOD::TrackParticleContainer>> tmpVect = tmpVertex->track_links();

            if(particleInCollection(tmpTrk, tmpVect)) myVtx = jfv;

        } // jfv
        jf_jet_trk_vertex.push_back(myVtx);

        TLorentzVector trk;
        trk.SetPtEtaPhiM(tmpTrk->pt(), tmpTrk->eta(), tmpTrk->phi(), track_mass);
        tracksTot4Mom += trk;

        TVector3 trkvector(0,0,0);
        trkvector = trk.Vect();

        float trackRapidity = (trkvector.Mag2()>0 ? tan(0.5 * trkvector.Angle(flightDir)) : 0);
        trackRapidity = (trackRapidity < 0.000001 ? (-1)*log(0.000001) : (-1)*log(trackRapidity) );
        sumTrackRapidity += trackRapidity;

        if(trkIndex == 0) {
            MaxTrkRapidity_jf_path = trackRapidity;
            MinTrkRapidity_jf_path = trackRapidity;
        }
        else {
            MaxTrkRapidity_jf_path = trackRapidity > MaxTrkRapidity_jf_path ? trackRapidity : MaxTrkRapidity_jf_path;
            MinTrkRapidity_jf_path = trackRapidity < MinTrkRapidity_jf_path ? trackRapidity : MinTrkRapidity_jf_path;
        }

        if(trkIndex == secondary_vertex_idx) {

            if(nTrk_vtx1 < 0){
                nTrk_vtx1 = 0;
            }

            nTrk_vtx1 += 1;

            tracksTot4Mom_firstVtx += trk;
            vtx1_sumTrackRapidity += trackRapidity;
            if(!vtx1_first_track){
                vtx1_MaxTrkRapidity_jf_path = trackRapidity;
                vtx1_MinTrkRapidity_jf_path = trackRapidity;
                vtx1_first_track=1;
            }
            else {
                vtx1_MaxTrkRapidity_jf_path = trackRapidity > vtx1_MaxTrkRapidity_jf_path ? trackRapidity : vtx1_MaxTrkRapidity_jf_path;
                vtx1_MinTrkRapidity_jf_path = trackRapidity < vtx1_MinTrkRapidity_jf_path ? trackRapidity : vtx1_MinTrkRapidity_jf_path;
            }
        } // == secondary

        if(myVtx == tertiary_vertex_idx){
            if(nTrk_vtx2 < 0){
                nTrk_vtx2 = 0;
            }

            nTrk_vtx2 += 1;

            tracksTot4Mom_secondVtx += trk;
            vtx2_sumTrackRapidity += trackRapidity;
            if(!vtx2_first_track){
                vtx2_MaxTrkRapidity_jf_path = trackRapidity;
                vtx2_MinTrkRapidity_jf_path = trackRapidity;
                vtx2_first_track=1;
            }
            else {
                vtx2_MaxTrkRapidity_jf_path = trackRapidity > vtx2_MaxTrkRapidity_jf_path ? trackRapidity : vtx2_MaxTrkRapidity_jf_path;
                vtx2_MinTrkRapidity_jf_path = trackRapidity < vtx2_MinTrkRapidity_jf_path ? trackRapidity : vtx2_MinTrkRapidity_jf_path;
            }
        } // == tertiary

        trkIndex++;
    } // tmpTrk

    AvgTrkRapidity_jf_path = trkIndex > 0 ? sumTrackRapidity / trkIndex : 0;

    if(nTrk_vtx1 > 0){
        JF_Lxy1 = closestVtx_L3D*sin(jf_theta);
        mass_first_vtx = tracksTot4Mom_firstVtx.M();
        e_first_vtx = tracksTot4Mom_firstVtx.E();
        e_frac_vtx1 = e_first_vtx/tracksTot4Mom.E();
        vtx1_AvgTrkRapidity_jf_path = vtx1_sumTrackRapidity/nTrk_vtx1;
    }
    if(nTrk_vtx2 > 0){
        JF_Lxy2 = second_closestVtx_L3D*sin(jf_theta);
        mass_second_vtx = tracksTot4Mom_secondVtx.M();
        e_second_vtx = tracksTot4Mom_secondVtx.E();
        e_frac_vtx2 = e_second_vtx/tracksTot4Mom.E();
        vtx2_AvgTrkRapidity_jf_path = vtx2_sumTrackRapidity/nTrk_vtx2;
    }

    if(type=="") {
        m_jf_PV_x = jf_PV_x;
        m_jf_PV_y = jf_PV_y;
        m_jf_PV_z = jf_PV_z;
    }
    else if(type=="em") {
        m_jf_PV_x_em = jf_PV_x;
        m_jf_PV_y_em = jf_PV_y;
        m_jf_PV_z_em = jf_PV_z;
    }
    else if(type=="tm") {
        m_jf_PV_x_tm = jf_PV_x;
        m_jf_PV_y_tm = jf_PV_y;
        m_jf_PV_z_tm = jf_PV_z;
    }
    

#define FILL_JFPROPERTY( type ) \
    do { \
        v_jf_jet_trk_vertex##type->push_back( jf_jet_trk_vertex ); \
        v_jf_jet_pb##type->push_back( jf_jet_pb ); \
        v_jf_jet_pc##type->push_back( jf_jet_pc ); \
        v_jf_jet_pu##type->push_back( jf_jet_pu ); \
        v_jf_jet_llr##type->push_back( jf_jet_llr ); \
        v_jf_jet_m##type->push_back( jf_jet_m * mev2gev ); \
        v_jf_jet_efc##type->push_back( jf_jet_efc ); \
        v_jf_jet_deta##type->push_back( jf_jet_deta ); \
        v_jf_jet_dphi##type->push_back( jf_jet_dphi ); \
        v_jf_jet_dR##type->push_back( jf_jet_dR ); \
        v_jf_jet_ntrkAtVx##type->push_back( jf_jet_ntrkAtVx ); \
        v_jf_jet_nvtx##type->push_back( jf_jet_nvtx ); \
        v_jf_jet_sig3d##type->push_back( jf_jet_sig3d ); \
        v_jf_jet_nvtx1t##type->push_back( jf_jet_nvtx1t ); \
        v_jf_jet_n2t##type->push_back( jf_jet_n2t ); \
        v_jf_jet_VTXsize##type->push_back( jf_jet_VTXsize ); \
        v_jf_jet_vtx_chi2##type->push_back( j_jf_vtx_chi2 ); \
        v_jf_jet_vtx_ndf##type->push_back( j_jf_vtx_ndf ); \
        v_jf_jet_vtx_ntrk##type->push_back( j_jf_vtx_ndf ); \
        v_jf_jet_vtx_L3d##type->push_back( j_jf_vtx_L3d ); \
        v_jf_jet_vtx_sig3d##type->push_back( j_jf_vtx_sig3d ); \
        v_jf_jet_vtx_x##type->push_back( j_jf_vtx_x ); \
        v_jf_jet_vtx_x_err##type->push_back( j_jf_vtx_x_err ); \
        v_jf_jet_vtx_y##type->push_back( j_jf_vtx_y ); \
        v_jf_jet_vtx_y_err##type->push_back( j_jf_vtx_y_err ); \
        v_jf_jet_vtx_z##type->push_back( j_jf_vtx_z ); \
        v_jf_jet_vtx_z_err##type->push_back( j_jf_vtx_z_err ); \
        v_jf_nTrk_vtx1##type->push_back( nTrk_vtx1 ); \
        v_jf_mass_first_vtx##type->push_back( mass_first_vtx * mev2gev ); \
        v_jf_e_first_vtx##type->push_back( e_first_vtx * mev2gev ); \
        v_jf_e_frac_vtx1##type->push_back( e_frac_vtx1 ); \
        v_jf_closestVtx_L3D##type->push_back( closestVtx_L3D ); \
        v_jf_JF_Lxy1##type->push_back( JF_Lxy1 ); \
        v_jf_vtx1_MaxTrkRapidity_jf_path##type->push_back( vtx1_MaxTrkRapidity_jf_path ); \
        v_jf_vtx1_AvgTrkRapidity_jf_path##type->push_back( vtx1_AvgTrkRapidity_jf_path ); \
        v_jf_vtx1_MinTrkRapidity_jf_path##type->push_back( vtx1_MinTrkRapidity_jf_path ); \
        v_jf_nTrk_vtx2##type->push_back( nTrk_vtx2 ); \
        v_jf_mass_second_vtx##type->push_back( mass_second_vtx * mev2gev ); \
        v_jf_e_second_vtx##type->push_back( e_second_vtx * mev2gev ); \
        v_jf_e_frac_vtx2##type->push_back( e_frac_vtx2  ); \
        v_jf_second_closestVtx_L3D##type->push_back( second_closestVtx_L3D ); \
        v_jf_JF_Lxy2##type->push_back( JF_Lxy2 ); \
        v_jf_vtx2_MaxTrkRapidity_jf_path##type->push_back( vtx2_MaxTrkRapidity_jf_path ); \
        v_jf_vtx2_AvgTrkRapidity_jf_path##type->push_back( vtx2_AvgTrkRapidity_jf_path ); \
        v_jf_vtx2_MinTrkRapidity_jf_path##type->push_back( vtx2_MinTrkRapidity_jf_path ); \
        v_jf_MaxTrkRapidity_jf_path##type->push_back( MaxTrkRapidity_jf_path ); \
        v_jf_MinTrkRapidity_jf_path##type->push_back( MinTrkRapidity_jf_path ); \
        v_jf_AvgTrkRapidity_jf_path##type->push_back( AvgTrkRapidity_jf_path ); \
    } while(0);

    if(type=="") {
        FILL_JFPROPERTY()
    }
    else if(type=="em") {
        FILL_JFPROPERTY(_em)
    }
    else if(type=="tm") {
        FILL_JFPROPERTY(_tm)
    }

    // IP stuff
    std::vector< ElementLink< xAOD::TrackParticleContainer > > IP2DTracks;
    IP2DTracks = btagging->auxdata<std::vector<ElementLink< xAOD::TrackParticleContainer> > >("IP2D_TrackParticleLinks");

    float ip2d_pb = -99.;
    float ip2d_pc = -99.;
    float ip2d_pu = -99.;
    float ip2d_llr = -99.;
    float ip3d_pb = -99.;
    float ip3d_pc = -99.;
    float ip3d_pu = -99.;
    float ip3d_llr = -99.;

    if(IP2DTracks.size() > 0) {
        ip2d_pb = btagging->IP2D_pb();
        ip2d_pc = btagging->IP2D_pc();
        ip2d_pu = btagging->IP2D_pu();
        ip2d_llr = btagging->IP2D_loglikelihoodratio();

        ip3d_pb = btagging->IP2D_pb();
        ip3d_pc = btagging->IP2D_pc();
        ip3d_pu = btagging->IP2D_pu();
        ip3d_llr = btagging->IP2D_loglikelihoodratio();

    } // track size > 0

    std::vector< ElementLink< xAOD::TrackParticleContainer > > IP3DTracks;
    IP3DTracks = btagging->auxdata<std::vector<ElementLink< xAOD::TrackParticleContainer> > >("IP3D_TrackParticleLinks");
    if(IP3DTracks.size() > 0) {
        ip3d_pb = btagging->IP3D_pb();
        ip3d_pc = btagging->IP3D_pc();
        ip3d_pu = btagging->IP3D_pu();
        ip3d_llr = btagging->IP3D_loglikelihoodratio();
    }
#define FILL_IPPROPERTY( type ) \
    do { \
        v_jet_ip2d_pb##type->push_back( ip2d_pb ); \
        v_jet_ip2d_pc##type->push_back( ip2d_pc ); \
        v_jet_ip2d_pu##type->push_back( ip2d_pu ); \
        v_jet_ip2d_llr##type->push_back( ip2d_llr ); \
        v_jet_ip3d_pb##type->push_back( ip3d_pb ); \
        v_jet_ip3d_pc##type->push_back( ip3d_pc ); \
        v_jet_ip3d_pu##type->push_back( ip3d_pu ); \
        v_jet_ip3d_llr##type->push_back( ip3d_llr ); \
    } while (0);

    if(type=="") {
        FILL_IPPROPERTY()
    }
    else if(type=="em") {
        FILL_IPPROPERTY(_em)
    }
    else if(type=="tm") {
        FILL_IPPROPERTY(_tm)
    }

#undef FILL_JETPROPERTY
#undef FILL_SV1PROPERTY
#undef FILL_JFPROPERTY
#undef FILL_IPPROPERTY
    
}

bool FTAGLooper::particleInCollection( const xAOD::TrackParticle *trkPart, std::vector< ElementLink< xAOD::TrackParticleContainer > > trkColl ) {
    for (unsigned int iT = 0; iT < trkColl.size(); iT++) {
        if (trkPart == *(trkColl.at(iT))) return true;
    }
    return false;
}

float FTAGLooper::JF_Transverse_error(float L3D, float Theta, float Theta_err, float Phi, float Phi_err){
  TVector3 vertexPos;
  vertexPos.SetMagThetaPhi(L3D,Theta,Phi);

  TVector3 vertexPos2;
  vertexPos2.SetMagThetaPhi(L3D,Theta+Theta_err,Phi);
  float Theta_err1 = fabs(L3D*TMath::Tan(vertexPos.Angle(vertexPos2)));

  vertexPos2.SetMagThetaPhi(L3D,Theta-Theta_err,Phi);
  float Theta_err2 = fabs(L3D*TMath::Tan(vertexPos.Angle(vertexPos2)));

  vertexPos2.SetMagThetaPhi(L3D,Theta,Phi+Phi_err);
  float Phi_err1 = fabs(L3D*TMath::Tan(vertexPos.Angle(vertexPos2)));

  vertexPos2.SetMagThetaPhi(L3D,Theta,Phi-Phi_err);
  float Phi_err2 = fabs(L3D*TMath::Tan(vertexPos.Angle(vertexPos2)));

  float transverse_Theta_error = std::max(Theta_err1,Theta_err2);
  float transverse_Phi_error = std::max(Phi_err1,Phi_err2);

  float transverse_err = TMath::Sqrt(transverse_Theta_error*transverse_Theta_error+transverse_Phi_error*transverse_Phi_error);

  return transverse_err;

}

std::vector<float> FTAGLooper::JF_xyz_errors(float L3D, float L3Derr, float Theta, float Theta_err, float Phi, float Phi_err,float Pv_x, float Pv_y, float Pv_z){

    TVector3 vertexPos;
    TVector3 vertexPos2;
    vertexPos.SetMagThetaPhi(L3D,Theta,Phi);

    //this is the relative position to primary vertex
    float x = vertexPos.X();
    float y = vertexPos.Y();
    float z = vertexPos.Z();

    vertexPos.SetMagThetaPhi(L3D+L3Derr,Theta,Phi);
    vertexPos2.SetMagThetaPhi(L3D-L3Derr,Theta,Phi);

    float L3D_x_err = std::max(fabs(vertexPos.X()-x),fabs(vertexPos2.X()-x));
    float L3D_y_err = std::max(fabs(vertexPos.Y()-y),fabs(vertexPos2.Y()-y));
    float L3D_z_err = std::max(fabs(vertexPos.Z()-z),fabs(vertexPos2.Z()-z));

    vertexPos.SetMagThetaPhi(L3D,Theta+Theta_err,Phi);
    vertexPos2.SetMagThetaPhi(L3D,Theta-Theta_err,Phi);

    float Theta_x_err = std::max(fabs(vertexPos.X()-x),fabs(vertexPos2.X()-x));
    float Theta_y_err = std::max(fabs(vertexPos.Y()-y),fabs(vertexPos2.Y()-y));
    float Theta_z_err = std::max(fabs(vertexPos.Z()-z),fabs(vertexPos2.Z()-z));
    vertexPos.SetMagThetaPhi(L3D,Theta,Phi+Phi_err);
    vertexPos2.SetMagThetaPhi(L3D,Theta,Phi-Phi_err);

    float Phi_x_err = std::max(fabs(vertexPos.X()-x),fabs(vertexPos2.X()-x));
    float Phi_y_err = std::max(fabs(vertexPos.Y()-y),fabs(vertexPos2.Y()-y));
    float Phi_z_err = std::max(fabs(vertexPos.Z()-z),fabs(vertexPos2.Z()-z));

    float x_err = TMath::Sqrt(L3D_x_err*L3D_x_err+Theta_x_err*Theta_x_err+Phi_x_err*Phi_x_err);
    float y_err = TMath::Sqrt(L3D_y_err*L3D_y_err+Theta_y_err*Theta_y_err+Phi_y_err*Phi_y_err);
    float z_err = TMath::Sqrt(L3D_z_err*L3D_z_err+Theta_z_err*Theta_z_err+Phi_z_err*Phi_z_err);

    //this is the x,y,z position relative to (0,0,0)
    x = Pv_x+x;
    y = Pv_y+y;
    z = Pv_z+z;

    std::vector<float> results;

    results.push_back(x);
    results.push_back(x_err);
    results.push_back(y);
    results.push_back(y_err);
    results.push_back(z);
    results.push_back(z_err);

    return results;
}
